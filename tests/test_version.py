from unittest import TestCase

from dq.version import version


class TestVersion(TestCase):
    def test_version(self):
        assert version == '0.0.1'
