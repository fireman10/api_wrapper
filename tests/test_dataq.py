import sys
from unittest import TestCase
from unittest.mock import patch

from dq import DQ, main
from dq.utils import DQAPIExplorer


class Testdq(TestCase):

    @patch('dq.utils.requests')
    def test_execute_flow(self, mock):
        creds = {
            'username': 'username',
            'password': 'password',
            'url': 'http://url',
        }
        message = "777"
        mock.get.return_value.json.return_value = {"execid": message}
        with patch.object(DQAPIExplorer, 'session_id', return_value='value'):
            assert mock.is_called()
            assert DQ(**creds).execute_flow(project_id='proj') == message

    @patch('dq.utils.requests')
    def test_get_execution_result(self, mock):
        creds = {
            'username': 'username',
            'password': 'password',
            'url': 'http://url',
        }
        message = "Passed"
        mock.post.return_value.text = f'{{"testStatus": {message}}}'
        mock.post.return_value.json.return_value = {"testStatus": message}
        with patch.object(DQAPIExplorer, 'session_id', return_value='value'):
            assert mock.is_called()
            assert DQ(**creds).get_result(execution_id='proj') == message


class TestParser(TestCase):

    @patch.object(DQ, 'execute_flow')
    @patch.object(DQ, 'get_result')
    def test_no_action(self, get_result_mock, execute_flow_mock):
        testargs = ["dtaq"]
        with patch.object(sys, 'argv', testargs):
            main()
            get_result_mock.assert_not_called()
            execute_flow_mock.assert_not_called()

    @patch.object(DQ, 'execute_flow', return_value='777')
    @patch.object(DQ, 'get_result')
    def test_execute_flow(self, get_result_mock, execute_flow_mock):
        testargs = ["dtaq", "execute", "-U", "usr", "-P", "psswd", "--url", "url", "--project", "proj"]
        message = "777"
        with patch.object(sys, 'argv', testargs):
            with patch('logging.info') as mock:
                main()
                mock.assert_called_with(message)
                execute_flow_mock.assert_called()
                get_result_mock.assert_not_called()

    @patch.object(DQ, 'execute_flow')
    @patch.object(DQ, 'get_result', return_value='PASSED')
    def test_get_result(self, get_result_mock, execute_flow_mock):
        testargs = ["dtaq", "result", "-U", "usr", "-P", "psswd", "--url", "url", "--execution_id", "777"]
        message = "PASSED"
        with patch.object(sys, 'argv', testargs):
            with patch('logging.info') as mock:
                main()
                mock.assert_called_with(message)
                get_result_mock.assert_called()
                execute_flow_mock.assert_not_called()
