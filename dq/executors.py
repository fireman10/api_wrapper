import logging
import time
import jmespath

from dq.utils import CacheManager
from dq.utils import DQAPIExplorer

class BaseExecutorError(Exception):
    pass


class ExecutorParamsValidationError(BaseExecutorError):
    pass


class BaseExecutor(object):

    def __init__(self, parser):
        self.parser = parser
        self.parser.set_defaults(which=self.__class__.parser_name())
        self.parser.add_argument("-U", "--username", help="Connector username", required=True)
        self.parser.add_argument("-P", "--password", help="Connector password", required=True)
        self.parser.add_argument("--url", help="API url", required=True)
        self.parser.add_argument("--group", help="Group name", required=True)
        self.parser.add_argument("--project", help="Project name", required=True)

    def init_cli_parser(self):
        raise NotImplementedError

    @staticmethod
    def execute(executor, *args, **kwargs):
        raise NotImplementedError

    @staticmethod
    def parser_name():
        return


class YMLBaseExecutor(object):

    def __init__(self, parser):
        self.parser = parser
        self.parser.set_defaults(which=self.__class__.parser_name())
        self.parser.add_argument("-U", "--username", help="Connector username", required=True)
        self.parser.add_argument("-P", "--password", help="Connector password", required=True)
        self.parser.add_argument("--log_level", help="logging level")
        self.parser.add_argument("--url", help="API url", required=True)


    def init_cli_parser(self):
        raise NotImplementedError

    @staticmethod
    def execute(executor, *args, **kwargs):
        raise NotImplementedError

    @staticmethod
    def parser_name():
        return


class LoadYaml(YMLBaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--yml", help="YAML file", required=True)

    @staticmethod
    def parser_name():
        return "load_yml"


class YamlResult(YMLBaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--yml", help="YAML file", required=True)
        self.parser.add_argument("--extended", type=bool, default=False, help="Get extended response")
        self.parser.add_argument("--timeout", type=int, default=600, help="timeout to fetch execution result")
        self.parser.add_argument("--sleep", type=int, default=15, help="time interval")

    @staticmethod
    def parser_name():
        return "yml_result"

    @staticmethod
    def execute(executor, *args, **kwargs):
        raise None

class TriggerFlowExecutor(BaseExecutor):

    def init_cli_parser(self):
        self.parser.add_argument("--project_ids", help="List of project id", required=True)
        self.parser.add_argument("--success_emails", help="Success emails (optional)")
        self.parser.add_argument("--failure_emails", help="Failure emails (optional)")
        self.parser.add_argument("--cache_result", type=bool, default=False, help="Save execution id(optional)")

    @staticmethod
    def execute(executor, project_id_list: list, success_emails=None,
                failure_emails=None, cache_result: bool = False, job=None, **kwargs):
        result = executor.execute_flow(project_id_list, successful_emails=success_emails, failure_emails=failure_emails,
                                       job=job)
        if cache_result:
            CacheManager.write(result['batchExecutionId'])
        return result['batch']['batchId'], result['batchExecutionId'] or result

    @staticmethod
    def execute_batch(executor, batch_id_list: list):
        batches_list = []
        for batch_id in batch_id_list:
            batch_execution_response = executor.execute_batch(batch_id)
            batches_list.append(batch_execution_response)
        return batches_list

    @staticmethod
    def parser_name():
        return "execute_flow"


class TriggerBatchExecutor(BaseExecutor):

    def init_cli_parser(self):
        self.parser.add_argument("--batch_ids", help="List of batch id", required=True)

    @staticmethod
    def parser_name():
        return "execute_batch"


class GetBatchExecutionStatusExecutor(BaseExecutor):

    def init_cli_parser(self):
        self.parser.add_argument("--batch_execution_id", type=str, help="Batch Execution Id")
        self.parser.add_argument("--extended", type=bool, default=False, help="Get extended response")
        self.parser.add_argument("--timeout", type=int, default=600, help="timeout to fetch execution result")
        self.parser.add_argument("--sleep", type=int, default=15, help="time interval")

    @staticmethod
    def execute(executor, batch_execution_id: str, extended: bool = False, timeout: int = 600, sleep: int = 15, **kwargs):
        execution_id = batch_execution_id or CacheManager.read()
        end_time = time.time() + timeout
        attempt = 1
        result = executor.get_execution_status(execution_id)
        while time.time() < end_time and result['batchExecutionSummary']['batchTestStatus'] == 'IN_PROGRESS':
            logging.info(f"Get flow result for {execution_id} (attempt {attempt})")
            result = executor.get_execution_status(execution_id)
            if result['batchExecutionSummary']['batchTestStatus'] == 'IN_PROGRESS':
                attempt += 1
                time.sleep(sleep)
        print("result")
        print(result)
        return result if extended else result['batchExecutionSummary']['batchTestStatus']

    @staticmethod
    def parser_name():
        return "execution status"


class GetResultExecutor(BaseExecutor):

    def init_cli_parser(self):
        self.parser.add_argument("--execution_id", type=int, help="Execution Id")

    @staticmethod
    def execute(executor, execution_id: int, **kwargs):
        execution_id = execution_id
        result = executor.get_result(execution_id)
        return result

    @staticmethod
    def parser_name():
        return "execution result"


class CompareSourcesExecutor(BaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--name", help="Execution Id")
        self.parser.add_argument("--source_connection_name", type=str, required=True, help="Source connection name")
        self.parser.add_argument("--source_schema_name", type=str, required=True, help="Source schema name")
        self.parser.add_argument("--target_connection_name", type=str, required=True, help="Target connection name")
        self.parser.add_argument("--target_schema_name", type=str, required=True, help="Target schema name")
        self.parser.add_argument("--tables_mapping", type=str, required=True,
                                 help="Tables mapping (format: s_table1->t_table1,s_table2->t_table2)")

    @staticmethod
    def execute(executor, yaml_sources: dict, yaml_targets: dict, job: dict):
        source_mapping = list(set(maps['source_name'] for maps in job['mapping']))
        target_mapping = list(set(maps['target_name'] for maps in job['mapping']))
        crunches = []
        if 'grouping' in job.keys() and job['grouping']:
            if len(source_mapping) > 1:
                raise ExecutorParamsValidationError("Multiple sources selected with grouping")
            elif len(target_mapping) > 1:
                raise ExecutorParamsValidationError("Multiple targets selected with grouping")
            if yaml_sources[source_mapping[0]]['source_type'] != 'table':
                raise ExecutorParamsValidationError("Grouping can only be used with source type: table")
            elif yaml_targets[target_mapping[0]]['target_type'] != 'table':
                raise ExecutorParamsValidationError("Grouping can only be used with target type: table")

            mapping = [(maps['source_data_set'], maps['target_data_set']) for maps in job['mapping']]
            grouped_mapping = DQAPIExplorer.get_table_grouping(executor, mapping,
                                                               yaml_sources[job['mapping'][0]['source_name']],
                                                               yaml_targets[job['mapping'][0]['target_name']], job)

            group_dict = {
                "medium_group": 0,
                "small_group": 0,
                "large_group": 0
            }

            for grouped_map in grouped_mapping:
                crunches_element = []
                for source, target in grouped_map[0]:
                    for maps in job['mapping']:
                        if maps['source_data_set'] == source and maps['target_data_set'] == target:
                            crunches_element.append(maps)

                crunches.append((crunches_element, f"{grouped_map[1]}_{group_dict[grouped_map[1]] + 1}"))
                group_dict[grouped_map[1]] += 1
        else:
            crunches = [(job['mapping'], '')]

        project_ids = []
        for crunch, flow_des_suffix in crunches:
            sources = {}
            targets = {}
            table_mapping = []
            for mapping in crunch:
                # checking if source type and target type matches
                if yaml_targets[mapping['target_name']]['target_type'] != yaml_sources[mapping['source_name']]['source_type']:
                    raise ExecutorParamsValidationError("Target and source should be of same type")
                # creating sources based on job mapping
                if mapping['source_name'] not in sources.keys():
                    sources[mapping['source_name']] = dict((i, yaml_sources[mapping['source_name']][i]) for i in
                                                            yaml_sources[mapping['source_name']] if i != 'data_set')
                    sources[mapping['source_name']]['data_set'] = []
                    sources[mapping['source_name']]['source_table_properties'] = []
                    sources[mapping['source_name']]['source_data_set_properties'] = {}

                # adding data set to the source
                for yaml_dataset in yaml_sources[mapping['source_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['source_data_set']:
                        sources[mapping['source_name']]['data_set'].append(yaml_dataset)


                if mapping['target_name'] not in targets.keys():
                    targets[mapping['target_name']] = dict((i, yaml_targets[mapping['target_name']][i]) for i in
                                                           yaml_targets[mapping['target_name']] if i != 'data_set')
                    targets[mapping['target_name']]['data_set'] = []
                    targets[mapping['target_name']]['target_table_properties'] = []
                    targets[mapping['target_name']]['target_data_set_properties'] = {}

                # adding data set to the target
                for yaml_dataset in yaml_targets[mapping['target_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['target_data_set']:
                        targets[mapping['target_name']]['data_set'].append(yaml_dataset)

                # component2 table mapping

                primary_key_source = []
                primary_key_target = []
                if yaml_sources[mapping['source_name']]['source_type'] == 'table':
                    res, primary_key_source, _ = CompareSourcesExecutor.get_table_info(executor, sources[mapping['source_name']]
                    ['connection_name'], sources[mapping['source_name']]['schema_name'], mapping['source_data_set'])

                    res, primary_key_target, _ = CompareSourcesExecutor.get_table_info(executor, targets[mapping['target_name']]
                    ['connection_name'], targets[mapping['target_name']]['schema_name'], mapping['target_data_set'])

                table_mapping_element = {
                    'srcDestTableMap': (mapping['source_data_set'], mapping['target_data_set']),
                    'compareKeyNew': (mapping.get('source_compare_keys').split(',') if mapping.get('source_compare_keys')
                    else list(set(primary_key_source)), mapping.get('source_compare_keys').split(',')
                    if mapping.get('source_compare_keys') else list(set(primary_key_source))),
                    'compareKey': mapping.get('compare_keys').split(',') if mapping.get('compare_keys')
                    else list(set(primary_key_source) & set(primary_key_target)),
                    'columnMapping': CompareSourcesExecutor.get_column_mapping(executor, yaml_sources[mapping['source_name']],
                                                                               yaml_targets[mapping['target_name']],
                                                                               (mapping['source_data_set'],
                                                                                mapping['target_data_set']), mapping)
                }

                table_mapping.append(table_mapping_element)

            for source in sources.keys():
                if sources[source]['source_type'] == 'table':
                    for dataset in sources[source]['data_set']:
                        res, primary_key, res1 = CompareSourcesExecutor.get_table_info(executor, sources
                        [source]['connection_name'], sources[source]['schema_name'], dataset['name'])
                        sources[source]['source_table_properties'].append(res)
                        sources[source]['source_data_set_properties'][dataset['name']] = res1

            for target in targets.keys():
                if targets[target]['target_type'] == 'table':
                    for dataset in targets[target]['data_set']:
                        res, primary_key, res1 = CompareSourcesExecutor.get_table_info(executor, targets
                        [target]['connection_name'], targets[target]['schema_name'], dataset['name'])
                        targets[target]['target_table_properties'].append(res)
                        targets[target]['target_data_set_properties'][dataset['name']] = res1

            project_id = executor.create_compare_flow(table_mapping, sources.values(), targets.values(), job,
                                                      flow_des_suffix)['project_id']
            project_ids.append(project_id)
        return project_ids

    @staticmethod
    def get_table_info(executor, connection_name, schema_name, table_name):
        keys_pathes = {
            "tblName": "keys(@)[0]",
            "tblSize": "{table_name}.tableSize",
            "tblRecordCount": "{table_name}.count",
        }
        result = executor.get_table_info(connection_name, schema_name, table_name)
        res = {k: jmespath.search(v.format(table_name=table_name), result) for k, v in keys_pathes.items()}
        res['tblSize'] = str(res['tblSize'])
        primary_key = jmespath.search(f'{table_name}.primaryKey', result)
        data_set_properties = None
        if result.get(table_name) and result[table_name].get('partitionColumn'):
            data_set_properties = {
                    "column": result[table_name].get('partitionColumn'),
                    "numberOfPart": result[table_name].get('noOfPartition')
            }
            if not data_set_properties['numberOfPart']:
                del data_set_properties['numberOfPart']

        return res, primary_key if primary_key else [], data_set_properties

    @staticmethod
    def get_column_mapping(executor,source: dict, target: dict, element: tuple, mapping: dict):
        source_element = element[0]
        target_element = element[1]
        source_exclude_column = []
        target_exclude_column = []

        source_data_set = [data_set['name'] for data_set in source['data_set']]
        target_data_set = [data_set['name'] for data_set in target['data_set']]

        if source_element not in source_data_set:
            raise ExecutorParamsValidationError(f" source dataset {source_element} not present in {source['name']}")
        if target_element not in target_data_set:
            raise ExecutorParamsValidationError(f" source dataset {target_element} not present in {target['name']}")

        if 'exclude' in mapping.keys():

            if 'source_columns' in mapping['exclude'].keys():
                source_exclude_column += mapping['exclude']['source_columns']
            if 'target_columns' in mapping['exclude'].keys():
                target_exclude_column += mapping['exclude']['target_columns']

        if source['source_type'] == 'sql':
            for data_set in source['data_set']:
                if data_set['name'] == element[0]:
                    source_element = data_set['query']
        if target['target_type'] == 'sql':
            for data_set in target['data_set']:
                if data_set['name'] == element[1]:
                    target_element = data_set['query']

        column_mapping = executor.get_column_mapping(source, target, (source_element, target_element),
                                                     source_exclude_column, target_exclude_column)
        return column_mapping

    @staticmethod
    def parser_name():
        return "compare"

    @staticmethod
    def validate_params(**kwargs):
        if not {
            'executor', 'name', 'source_connection_name', 'source_schema_name',
            'target_connection_name', 'target_schema_name', 'tables_mapping'
        }.issubset({k for k, v in kwargs.items() if v}):
            raise ExecutorParamsValidationError

        for i in kwargs.get('tables_mapping').split(','):
            if '->' not in i:
                raise ExecutorParamsValidationError("Mapping source->target tables should be joined with '->'")


class schema_comapre(BaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--name", help="Execution Id")
        self.parser.add_argument("--source_connection_name", type=str, required=True, help="Source connection name")
        self.parser.add_argument("--source_schema_name", type=str, required=True, help="Source schema name")
        self.parser.add_argument("--target_connection_name", type=str, required=True, help="Target connection name")
        self.parser.add_argument("--target_schema_name", type=str, required=True, help="Target schema name")
        self.parser.add_argument("--tables_mapping", type=str, required=True,
                                 help="Tables mapping (format: s_table1->t_table1,s_table2->t_table2)")

    @staticmethod
    def execute(executor, yaml_sources: dict, yaml_targets: dict, job: dict):
        source_mapping = list(set(maps['source_name'] for maps in job['mapping']))
        target_mapping = list(set(maps['target_name'] for maps in job['mapping']))
        crunches = []
        if 'grouping' in job.keys() and job['grouping']:
            if len(source_mapping) > 1:
                raise ExecutorParamsValidationError("Multiple sources selected with grouping")
            elif len(target_mapping) > 1:
                raise ExecutorParamsValidationError("Multiple targets selected with grouping")
            if yaml_sources[source_mapping[0]]['source_type'] != 'table':
                raise ExecutorParamsValidationError("Grouping can only be used with source type: table")
            elif yaml_targets[target_mapping[0]]['target_type'] != 'table':
                raise ExecutorParamsValidationError("Grouping can only be used with target type: table")

            mapping = [(maps['source_data_set'], maps['target_data_set']) for maps in job['mapping']]
            grouped_mapping = DQAPIExplorer.get_table_grouping(executor, mapping,
                                                               yaml_sources[job['mapping'][0]['source_name']],
                                                               yaml_targets[job['mapping'][0]['target_name']], job)

            group_dict = {
                "medium_group": 0,
                "small_group": 0,
                "large_group": 0
            }

            for grouped_map in grouped_mapping:
                crunches_element = []
                for source, target in grouped_map[0]:
                    for maps in job['mapping']:
                        if maps['source_data_set'] == source and maps['target_data_set'] == target:
                            crunches_element.append(maps)

                crunches.append((crunches_element, f"{grouped_map[1]}_{group_dict[grouped_map[1]] + 1}"))
                group_dict[grouped_map[1]] += 1
        else:
            crunches = [(job['mapping'], '')]

        project_ids = []
        for crunch, flow_des_suffix in crunches:
            sources = {}
            targets = {}
            table_mapping = []
            for mapping in crunch:
                # checking if source type and target type matches
                if yaml_targets[mapping['target_name']]['target_type'] != yaml_sources[mapping['source_name']]['source_type']:
                    raise ExecutorParamsValidationError("Target and source should be of same type")
                # creating sources based on job mapping
                if mapping['source_name'] not in sources.keys():
                    sources[mapping['source_name']] = dict((i, yaml_sources[mapping['source_name']][i]) for i in
                                                            yaml_sources[mapping['source_name']] if i != 'data_set')
                    sources[mapping['source_name']]['data_set'] = []
                    sources[mapping['source_name']]['source_table_properties'] = []
                    sources[mapping['source_name']]['source_data_set_properties'] = {}

                # adding data set to the source
                for yaml_dataset in yaml_sources[mapping['source_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['source_data_set']:
                        sources[mapping['source_name']]['data_set'].append(yaml_dataset)


                if mapping['target_name'] not in targets.keys():
                    targets[mapping['target_name']] = dict((i, yaml_targets[mapping['target_name']][i]) for i in
                                                           yaml_targets[mapping['target_name']] if i != 'data_set')
                    targets[mapping['target_name']]['data_set'] = []
                    targets[mapping['target_name']]['target_table_properties'] = []
                    targets[mapping['target_name']]['target_data_set_properties'] = {}

                # adding data set to the target
                for yaml_dataset in yaml_targets[mapping['target_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['target_data_set']:
                        targets[mapping['target_name']]['data_set'].append(yaml_dataset)

                # component2 table mapping

                primary_key_source = []
                primary_key_target = []
                if yaml_sources[mapping['source_name']]['source_type'] == 'table':
                    res, primary_key_source, _ = schema_comapre.get_table_info(executor, sources[mapping['source_name']]
                    ['connection_name'], sources[mapping['source_name']]['schema_name'], mapping['source_data_set'])

                    res, primary_key_target, _ = schema_comapre.get_table_info(executor, targets[mapping['target_name']]
                    ['connection_name'], targets[mapping['target_name']]['schema_name'], mapping['target_data_set'])

                table_mapping_element = {
                    'srcDestTableMap': (mapping['source_data_set'], mapping['target_data_set']),
                    'compareKeyNew': (mapping.get('source_compare_keys').split(',') if mapping.get('source_compare_keys')
                    else list(set(primary_key_source)), mapping.get('source_compare_keys').split(',')
                    if mapping.get('source_compare_keys') else list(set(primary_key_source))),
                    'compareKey': mapping.get('compare_keys').split(',') if mapping.get('compare_keys')
                    else list(set(primary_key_source) & set(primary_key_target)),
                    # 'columnMapping': schema_comapre.get_column_mapping(executor, yaml_sources[mapping['source_name']],
                    #                                                            yaml_targets[mapping['target_name']],
                    #                                                            (mapping['source_data_set'],
                    #                                                             mapping['target_data_set']), mapping)
                    'columnMapping': []
                }

                table_mapping.append(table_mapping_element)

            for source in sources.keys():
                if sources[source]['source_type'] == 'table':
                    for dataset in sources[source]['data_set']:
                        res, primary_key, res1 = schema_comapre.get_table_info(executor, sources
                        [source]['connection_name'], sources[source]['schema_name'], dataset['name'])
                        sources[source]['source_table_properties'].append(res)
                        sources[source]['source_data_set_properties'][dataset['name']] = res1

            for target in targets.keys():
                if targets[target]['target_type'] == 'table':
                    for dataset in targets[target]['data_set']:
                        res, primary_key, res1 = schema_comapre.get_table_info(executor, targets
                        [target]['connection_name'], targets[target]['schema_name'], dataset['name'])
                        targets[target]['target_table_properties'].append(res)
                        targets[target]['target_data_set_properties'][dataset['name']] = res1

            project_id = executor.create_schema_compare_flow(table_mapping, sources.values(), targets.values(), job,
                                                      flow_des_suffix)['project_id']
            project_ids.append(project_id)
        return project_ids

    @staticmethod
    def get_table_info(executor, connection_name, schema_name, table_name):
        keys_pathes = {
            "tblName": "keys(@)[0]",
            "tblSize": "{table_name}.tableSize",
            "tblRecordCount": "{table_name}.count",
            "indexInfo": "{table_name}.indexInfo",
            "foreignKey": "{table_name}.foreignKey",
            "primaryKeys": "{table_name}.primaryKeys",
        }
        result = executor.get_table_info(connection_name, schema_name, table_name, True)
        res = {k: jmespath.search(v.format(table_name=table_name), result) for k, v in keys_pathes.items()}
        res['tblSize'] = str(res['tblSize'])
        primary_key = jmespath.search(f'{table_name}.primaryKey', result)
        data_set_properties = None
        if result.get(table_name) and result[table_name].get('partitionColumn'):
                data_set_properties = {
                    "column": result[table_name].get('partitionColumn'),
                    "numberOfPart": result[table_name].get('noOfPartition')
                }
                if not data_set_properties['numberOfPart']:
                    del data_set_properties['numberOfPart']

        return res, primary_key if primary_key else [], data_set_properties

    @staticmethod
    def get_column_mapping(executor,source: dict, target: dict, element: tuple, mapping: dict):
        source_element = element[0]
        target_element = element[1]
        source_exclude_column = []
        target_exclude_column = []

        source_data_set = [data_set['name'] for data_set in source['data_set']]
        target_data_set = [data_set['name'] for data_set in target['data_set']]

        if source_element not in source_data_set:
            raise ExecutorParamsValidationError(f" source dataset {source_element} not present in {source['name']}")
        if target_element not in target_data_set:
            raise ExecutorParamsValidationError(f" source dataset {target_element} not present in {target['name']}")

        if 'exclude' in mapping.keys():

            if 'source_columns' in mapping['exclude'].keys():
                source_exclude_column += mapping['exclude']['source_columns']
            if 'target_columns' in mapping['exclude'].keys():
                target_exclude_column += mapping['exclude']['target_columns']

        if source['source_type'] == 'sql':
            for data_set in source['data_set']:
                if data_set['name'] == element[0]:
                    source_element = data_set['query']
        if target['target_type'] == 'sql':
            for data_set in target['data_set']:
                if data_set['name'] == element[1]:
                    target_element = data_set['query']

        column_mapping = executor.get_column_mapping(source, target, (source_element, target_element),
                                                     source_exclude_column, target_exclude_column)
        return column_mapping

    @staticmethod
    def parser_name():
        return "compare"

    @staticmethod
    def validate_params(**kwargs):
        if not {
            'executor', 'name', 'source_connection_name', 'source_schema_name',
            'target_connection_name', 'target_schema_name', 'tables_mapping'
        }.issubset({k for k, v in kwargs.items() if v}):
            raise ExecutorParamsValidationError

        for i in kwargs.get('tables_mapping').split(','):
            if '->' not in i:
                raise ExecutorParamsValidationError("Mapping source->target tables should be joined with '->'")


class EtlTestingExecutor(BaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--name", help="Execution Id")
        self.parser.add_argument("--source_connection_name", type=str, required=True, help="Source connection name")
        self.parser.add_argument("--source_schema_name", type=str, required=True, help="Source schema name")
        self.parser.add_argument("--target_connection_name", type=str, required=True, help="Target connection name")
        self.parser.add_argument("--target_schema_name", type=str, required=True, help="Target schema name")
        self.parser.add_argument("--tables_mapping", type=str, required=True,
                                 help="Tables mapping (format: s_table1->t_table1,s_table2->t_table2)")

    @staticmethod
    def execute(executor, yaml_sources: dict, yaml_targets: dict, job: dict):
        source_mapping = list(set(maps['source_name'] for maps in job['mapping']))
        target_mapping = list(set(maps['target_name'] for maps in job['mapping']))
        crunches = []
        if 'grouping' in job.keys() and job['grouping']:
            if len(source_mapping) > 1:
                raise ExecutorParamsValidationError("Multiple sources selected with grouping")
            elif len(target_mapping) > 1:
                raise ExecutorParamsValidationError("Multiple targets selected with grouping")
            if yaml_sources[source_mapping[0]]['source_type'] != 'table':
                raise ExecutorParamsValidationError("Grouping can only be used with source type: table")
            elif yaml_targets[target_mapping[0]]['target_type'] != 'table':
                raise ExecutorParamsValidationError("Grouping can only be used with target type: table")

            mapping = [(maps['source_data_set'], maps['target_data_set']) for maps in job['mapping']]
            grouped_mapping = DQAPIExplorer.get_table_grouping(executor, mapping,
                                                               yaml_sources[job['mapping'][0]['source_name']],
                                                               yaml_targets[job['mapping'][0]['target_name']], job)

            group_dict = {
                "medium_group": 0,
                "small_group": 0,
                "large_group": 0
            }

            for grouped_map in grouped_mapping:
                crunches_element = []
                for source, target in grouped_map[0]:
                    for maps in job['mapping']:
                        if maps['source_data_set'] == source and maps['target_data_set'] == target:
                            crunches_element.append(maps)

                crunches.append((crunches_element, f"{grouped_map[1]}_{group_dict[grouped_map[1]] + 1}"))
                group_dict[grouped_map[1]] += 1
        else:
            crunches = [(job['mapping'], '')]

        project_ids = []
        for crunch, flow_des_suffix in crunches:
            sources = {}
            targets = {}
            table_mapping = []
            for mapping in crunch:
                # checking if source type and target type matches
                if yaml_targets[mapping['target_name']]['target_type'] != yaml_sources[mapping['source_name']]['source_type']:
                    raise ExecutorParamsValidationError("Target and source should be of same type")
                # creating sources based on job mapping
                if mapping['source_name'] not in sources.keys():
                    sources[mapping['source_name']] = dict((i, yaml_sources[mapping['source_name']][i]) for i in
                                                            yaml_sources[mapping['source_name']] if i != 'data_set')
                    sources[mapping['source_name']]['data_set'] = []
                    sources[mapping['source_name']]['source_table_properties'] = []
                    sources[mapping['source_name']]['source_data_set_properties'] = {}

                # adding data set to the source
                for yaml_dataset in yaml_sources[mapping['source_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['source_data_set']:
                        sources[mapping['source_name']]['data_set'].append(yaml_dataset)


                if mapping['target_name'] not in targets.keys():
                    targets[mapping['target_name']] = dict((i, yaml_targets[mapping['target_name']][i]) for i in
                                                           yaml_targets[mapping['target_name']] if i != 'data_set')
                    targets[mapping['target_name']]['data_set'] = []
                    targets[mapping['target_name']]['target_table_properties'] = []
                    targets[mapping['target_name']]['target_data_set_properties'] = {}

                # adding data set to the target
                for yaml_dataset in yaml_targets[mapping['target_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['target_data_set']:
                        targets[mapping['target_name']]['data_set'].append(yaml_dataset)

                # component2 table mapping

                primary_key_source = []
                primary_key_target = []
                if yaml_sources[mapping['source_name']]['source_type'] == 'table':
                    res, primary_key_source, _ = CompareSourcesExecutor.get_table_info(executor, sources[mapping['source_name']]
                    ['connection_name'], sources[mapping['source_name']]['schema_name'], mapping['source_data_set'])

                    res, primary_key_target, _ = CompareSourcesExecutor.get_table_info(executor, targets[mapping['target_name']]
                    ['connection_name'], targets[mapping['target_name']]['schema_name'], mapping['target_data_set'])

                table_mapping_element = {
                    'srcDestTableMap': (mapping['source_data_set'], mapping['target_data_set']),
                    'compareKeyNew': (mapping.get('source_compare_keys').split(',') if mapping.get('source_compare_keys')
                    else list(set(primary_key_source)), mapping.get('source_compare_keys').split(',')
                    if mapping.get('source_compare_keys') else list(set(primary_key_source))),
                    'compareKey': mapping.get('compare_keys').split(',') if mapping.get('compare_keys')
                    else list(set(primary_key_source) & set(primary_key_target)),
                    'columnMapping': CompareSourcesExecutor.get_column_mapping(executor, yaml_sources[mapping['source_name']],
                                                                               yaml_targets[mapping['target_name']],
                                                                               (mapping['source_data_set'],
                                                                                mapping['target_data_set']), mapping)
                }

                table_mapping.append(table_mapping_element)

            for source in sources.keys():
                if sources[source]['source_type'] == 'table':
                    for dataset in sources[source]['data_set']:
                        res, primary_key, res1 = CompareSourcesExecutor.get_table_info(executor, sources
                        [source]['connection_name'], sources[source]['schema_name'], dataset['name'])
                        sources[source]['source_table_properties'].append(res)
                        sources[source]['source_data_set_properties'][dataset['name']] = res1

            for target in targets.keys():
                if targets[target]['target_type'] == 'table':
                    for dataset in targets[target]['data_set']:
                        res, primary_key, res1 = CompareSourcesExecutor.get_table_info(executor, targets
                        [target]['connection_name'], targets[target]['schema_name'], dataset['name'])
                        targets[target]['target_table_properties'].append(res)
                        targets[target]['target_data_set_properties'][dataset['name']] = res1

            project_id = executor.create_etl_flow(table_mapping, sources.values(), targets.values(), job,
                                                      flow_des_suffix)['project_id']
            project_ids.append(project_id)
        return project_ids

    @staticmethod
    def get_table_info(executor, connection_name, schema_name, table_name):
        keys_pathes = {
            "tblName": "keys(@)[0]",
            "tblSize": "{table_name}.tableSize",
            "tblRecordCount": "{table_name}.count",
        }
        result = executor.get_table_info(connection_name, schema_name, table_name)
        res = {k: jmespath.search(v.format(table_name=table_name), result) for k, v in keys_pathes.items()}
        res['tblSize'] = str(res['tblSize'])
        primary_key = jmespath.search(f'{table_name}.primaryKey', result)
        return res, primary_key if primary_key else []

    @staticmethod
    def get_column_mapping(executor,source: dict, target: dict, element: tuple, mapping: dict):
        source_element = element[0]
        target_element = element[1]
        source_exclude_column = []
        target_exclude_column = []

        source_data_set = [data_set['name'] for data_set in source['data_set']]
        target_data_set = [data_set['name'] for data_set in target['data_set']]

        if source_element not in source_data_set:
            raise ExecutorParamsValidationError(f" source dataset {source_element} not present in {source['name']}")
        if target_element not in target_data_set:
            raise ExecutorParamsValidationError(f" source dataset {target_element} not present in {target['name']}")

        if 'exclude' in mapping.keys():

            if 'source_columns' in mapping['exclude'].keys():
                source_exclude_column += mapping['exclude']['source_columns']
            if 'target_columns' in mapping['exclude'].keys():
                target_exclude_column += mapping['exclude']['target_columns']

        if source['source_type'] == 'sql':
            for data_set in source['data_set']:
                if data_set['name'] == element[0]:
                    source_element = data_set['query']
        if target['target_type'] == 'sql':
            for data_set in target['data_set']:
                if data_set['name'] == element[1]:
                    target_element = data_set['query']

        column_mapping = executor.get_column_mapping(source, target, (source_element, target_element),
                                                     source_exclude_column, target_exclude_column)
        return column_mapping

    @staticmethod
    def parser_name():
        return "compare"

    @staticmethod
    def validate_params(**kwargs):
        if not {
            'executor', 'name', 'source_connection_name', 'source_schema_name',
            'target_connection_name', 'target_schema_name', 'tables_mapping'
        }.issubset({k for k, v in kwargs.items() if v}):
            raise ExecutorParamsValidationError

        for i in kwargs.get('tables_mapping').split(','):
            if '->' not in i:
                raise ExecutorParamsValidationError("Mapping source->target tables should be joined with '->'")


class DataProfileExecutor(BaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--name", help="Execution Id")
        self.parser.add_argument("--source_connection_name", type=str, required=True, help="Source connection name")
        self.parser.add_argument("--source_schema_name", type=str, required=True, help="Source schema name")
        self.parser.add_argument("--tables_mapping", type=str, required=True,
                                 help="Tables mapping (format: s_table1->t_table1,s_table2->t_table2)")

    @staticmethod
    def execute(executor, yaml_sources: dict, job: dict):
        source_mapping = list(set(maps['source_name'] for maps in job['mapping']))

        crunches = [(job['mapping'], '')]

        project_ids = []
        for crunch, flow_des_suffix in crunches:
            sources = {}
            targets = {}
            table_mapping = []
            for mapping in crunch:
                # creating sources based on job mapping
                if mapping['source_name'] not in sources.keys():
                    sources[mapping['source_name']] = dict((i, yaml_sources[mapping['source_name']][i]) for i in
                                                            yaml_sources[mapping['source_name']] if i != 'data_set')
                    sources[mapping['source_name']]['data_set'] = []
                    sources[mapping['source_name']]['source_table_properties'] = []

                # adding data set to the source
                for yaml_dataset in yaml_sources[mapping['source_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['source_data_set']:
                        sources[mapping['source_name']]['data_set'].append(yaml_dataset)

            for source in sources.keys():
                if sources[source]['source_type'] == 'table':
                    for dataset in sources[source]['data_set']:
                        res, primary_key = DataProfileExecutor.get_table_info(executor, sources
                        [source]['connection_name'], sources[source]['schema_name'], dataset['name'])
                        sources[source]['source_table_properties'].append(res)

            project_id = executor.create_data_profile_flow(sources.values(), job)['project_id']
            project_ids.append(project_id)
        return project_ids

    @staticmethod
    def get_table_info(executor, connection_name, schema_name, table_name):
        keys_pathes = {
            "tblName": "keys(@)[0]",
            "tblSize": "{table_name}.tableSize",
            "tblRecordCount": "{table_name}.count",
        }
        result = executor.get_table_info(connection_name, schema_name, table_name)
        res = {k: jmespath.search(v.format(table_name=table_name), result) for k, v in keys_pathes.items()}
        res['tblSize'] = str(res['tblSize'])
        primary_key = jmespath.search(f'{table_name}.primaryKey', result)
        return res, primary_key if primary_key else []

    @staticmethod
    def parser_name():
        return "data profile"

    @staticmethod
    def validate_params(**kwargs):
        if not {
            'executor', 'name', 'source_connection_name', 'source_schema_name', 'tables_mapping'
        }.issubset({k for k, v in kwargs.items() if v}):
            raise ExecutorParamsValidationError

        for i in kwargs.get('tables_mapping').split(','):
            if '->' not in i:
                raise ExecutorParamsValidationError("Mapping source->target tables should be joined with '->'")



class DataQualityExecutor(BaseExecutor):
    def init_cli_parser(self):
        self.parser.add_argument("--name", help="Execution Id")
        self.parser.add_argument("--source_connection_name", type=str, required=True, help="Source connection name")
        self.parser.add_argument("--source_schema_name", type=str, required=True, help="Source schema name")
        self.parser.add_argument("--tables_mapping", type=str, required=True,
                                 help="Tables mapping (format: s_table1->t_table1,s_table2->t_table2)")

    @staticmethod
    def execute(executor, yaml_sources: dict, job: dict):
        crunches = [(job['mapping'], '')]

        project_ids = []
        for crunch, flow_des_suffix in crunches:
            sources = {}
            for mapping in crunch:
                # creating sources based on job mapping
                if mapping['source_name'] not in sources.keys():
                    sources[mapping['source_name']] = dict((i, yaml_sources[mapping['source_name']][i]) for i in
                                                            yaml_sources[mapping['source_name']] if i != 'data_set')
                    sources[mapping['source_name']]['data_set'] = []
                    sources[mapping['source_name']]['source_table_properties'] = []

                # adding data set to the source
                for yaml_dataset in yaml_sources[mapping['source_name']]['data_set']:
                    if yaml_dataset['name'] == mapping['source_data_set']:
                        sources[mapping['source_name']]['data_set'].append(yaml_dataset)

            for source in sources.keys():
                if sources[source]['source_type'] == 'table':
                    for dataset in sources[source]['data_set']:
                        res, primary_key = DataQualityExecutor.get_table_info(executor, sources
                        [source]['connection_name'], sources[source]['schema_name'], dataset['name'])
                        sources[source]['source_table_properties'].append(res)

            project_id = executor.create_data_quality_flow(sources.values(), job)['project_id']
            project_ids.append(project_id)
        return project_ids

    @staticmethod
    def get_table_info(executor, connection_name, schema_name, table_name):
        keys_pathes = {
            "tblName": "keys(@)[0]",
            "tblSize": "{table_name}.tableSize",
            "tblRecordCount": "{table_name}.count",
        }
        result = executor.get_table_info(connection_name, schema_name, table_name)
        res = {k: jmespath.search(v.format(table_name=table_name), result) for k, v in keys_pathes.items()}
        res['tblSize'] = str(res['tblSize'])
        primary_key = jmespath.search(f'{table_name}.primaryKey', result)
        return res, primary_key if primary_key else []

    @staticmethod
    def parser_name():
        return "data profile"

    @staticmethod
    def validate_params(**kwargs):
        if not {
            'executor', 'name', 'source_connection_name', 'source_schema_name', 'tables_mapping'
        }.issubset({k for k, v in kwargs.items() if v}):
            raise ExecutorParamsValidationError

        for i in kwargs.get('tables_mapping').split(','):
            if '->' not in i:
                raise ExecutorParamsValidationError("Mapping source->target tables should be joined with '->'")
