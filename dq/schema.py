{
    "type" : "object",
    "properties" : {
        "group" : {
            "type" : "string",
            "description": "Group name"
        },
        "project" : {
            "type" : "string",
            "description": "Project name"
        },
        "version" : {
            "type" : "string",
            "description": "YAML name"
        },
        "source" : {
            "type" : "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Source name"
                    },
                    "source_type": {
                        "type": "string",
                        "description": "Source type",
                        "enum": [
                            "table",
                            "sql",
                            "file"
                        ]
                    },
                    "connection_name": {
                        "type": "string",
                        "description": "Connection name"
                    },
                    "source_format": {
                        "type": "string",
                        "description": "source format",
                        "enum": [
                            "PARQUET",
                            "CSV",
                            "EXCEL",
                            "JSON"
                        ]
                    },
                    "source_path": {
                        "type": "string",
                        "description": "source path"
                    },
                    "delimiter": {
                        "type": "string",
                        "description": "csv delimiter"
                    },
                    "header": {
                        "type": "boolean",
                        "description": "true if header is present else false"
                    },
                    "inferSchema": {
                        "type": "boolean",
                        "description": "true if infer schema else false"
                    },
                    "multiline": {
                        "type": "boolean",
                        "description": "true if multiline else false"
                    },
                    "columns": {
                        "type": "string",
                        "description": "Comma-separated column name"
                    },
                    "flatten_data": {
                        "type": "boolean",
                        "description": "true is flatten data else false"
                    }
                },
                "allOf": [
                    {
                        "if": {
                            "properties": {
                                "source_type": {
                                    "const": "sql"
                                }
                            }
                        },
                        "then": {
                            "properties": {
                                "data_set": {
                                    "type": "array",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "description": "data set name"
                                            },
                                            "query": {
                                                "type": "string",
                                                "description": "data set name"
                                            }
                                        },
                                        "required": [
                                            "name",
                                            "query"
                                        ]
                                    }
                                }
                            },
                            "required": [
                                "name",
                                "source_type",
                                "connection_name",
                                "schema_name"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "source_type": {
                                    "const": "table"
                                }
                            }
                        },
                        "then": {
                            "properties": {
                                "data_set": {
                                    "type": "array",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "description": "table name"
                                            }
                                        },
                                        "required": [
                                            "name"
                                        ]
                                    }
                                }
                            },
                            "required": [
                                "name",
                                "source_type",
                                "connection_name",
                                "schema_name"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "source_type": {
                                    "const": "file"
                                }
                            },
                            "required": ["source_type"]
                        },
                        "then":
                        {
                            "required": [
                                "name",
                                "source_type",
                                "source_format",
                                "source_path"

                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "source_format": {
                                    "const": "PARQUET"
                                }
                            },
                            "required": ["source_format"]
                        },
                        "then": {
                            "required": [
                                "name",
                                "source_type",
                                "source_format",
                                "source_path"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "source_format": {
                                    "const": "CSV"
                                }
                            },
                            "required": ["source_format"]
                        },
                        "then": {
                            "required": [
                                "name",
                                "source_type",
                                "source_format",
                                "source_path",
                                "delimiter",
                                "header",
                                "inferSchema"

                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "source_format": {
                                    "const": "EXCEL"
                                }
                            },
                            "required": ["source_format"]

                        },
                        "then": {
                            "required": [
                                "name",
                                "source_type",
                                "source_format",
                                "source_path",
                                "header",
                                "inferSchema"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "source_format": {
                                    "const": "JSON"
                                }
                            },
                            "required": ["source_format"]
                        },
                        "then":
                        {
                            "required": [
                                    "name",
                                    "source_type",
                                    "source_format",
                                    "source_path",
                                    "multiline"

                                ]
                        }
                    }
                ],
                "required": ["name", "source_type"]
            },
            "description": "List of sources name"
        },
        "target" : {
            "type" : "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Target name"
                    },
                    "target_type": {
                        "type": "string",
                        "description": "Target type",
                        "enum": [
                            "table",
                            "sql",
                            "file"
                        ]
                    },
                    "target_format": {
                        "type": "string",
                        "description": "Target type",
                        "enum": [
                            "PARQUET",
                            "JSON",
                            "CSV",
                            "EXCEL"
                        ]
                    },
                    "connection_name": {
                        "type": "string",
                        "description": "Connection name"
                    },
                    "schema_name": {
                        "type": "string",
                        "description": "Connection name"
                    }
                },
                "allOf": [
                    {
                        "if": {
                            "properties": {
                                "target_type": {
                                    "const": "sql"
                                }
                            },
                            "required": ["target_type"]
                        },
                        "then": {
                            "properties": {
                                "data_set": {
                                    "type": "array",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "description": "data set name"
                                            },
                                            "query": {
                                                "type": "string",
                                                "description": "data set name"
                                            }
                                        },
                                        "required": [
                                            "name",
                                            "query"
                                        ]
                                    }
                                }
                            },
                            "required": [
                                "name",
                                "target_type",
                                "connection_name",
                                "schema_name"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "target_type": {
                                    "const": "table"
                                }
                            },
                            "required": [
                                            "target_type"
                                        ]
                        },
                        "then": {
                            "properties": {
                                "data_set": {
                                    "type": "array",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "description": "table name"
                                            }
                                        },
                                        "required": [
                                            "name"
                                        ]
                                    }
                                }
                            },
                            "required": [
                                "name",
                                "target_type",
                                "connection_name",
                                "schema_name"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "target_type": {
                                    "const": "file"
                                }
                            },
                            "required": ["target_type"]
                        },
                        "then":
                        {
                            "required": [
                                "name",
                                "target_type",
                                "target_format",
                                "target_path"

                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "target_format": {
                                    "const": "PARQUET"
                                }
                            },
                            "required": ["target_format"]
                        },
                        "then": {
                            "required": [
                                "name",
                                "target_type",
                                "target_format",
                                "target_path"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "target_format": {
                                    "const": "CSV"
                                }
                            },
                            "required": ["target_format"]
                        },
                        "then": {
                            "required": [
                                "name",
                                "target_type",
                                "target_format",
                                "target_path",
                                "delimiter",
                                "header",
                                "inferSchema"

                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "target_format": {
                                    "const": "EXCEL"
                                }
                            },
                            "required": ["target_format"]

                        },
                        "then": {
                            "required": [
                                "name",
                                "target_type",
                                "target_format",
                                "target_path",
                                "header",
                                "inferSchema"
                            ]
                        }
                    },
                    {
                        "if": {
                            "properties": {
                                "target_format": {
                                    "const": "JSON"
                                }
                            },
                            "required": ["target_format"]
                        },
                        "then":
                        {
                            "required": [
                                    "name",
                                    "target_type",
                                    "target_format",
                                    "target_path",
                                    "multiline"

                                ]
                        }
                    }
                ]
            },
            "description": "List of target name"
        },
        "job" :{
            "type" : "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Job name"
                    },
                    "job_type": {
                        "type": "string",
                        "enum": [
                            "data migration validation",
                            "data profile",
                            "etl testing",
                            "data quality",
                            "schema compare"
                        ],
                        "description": "job type"
                    },
                    "mapping": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "source_name": {
                                    "type": "string",
                                    "description": "source name"
                                },
                                "target_name": {
                                    "type": "string",
                                    "description": "target name"
                                },
                                "source_data_set": {
                                    "type": "string",
                                    "description": "source data set name"
                                },
                                "target_data_set": {
                                    "type": "string",
                                    "description": "target data set name"
                                },
                                "exclude": {
                                    "type": "object",
                                    "properties": {
                                        "source_columns": {
                                            "type": "array",
                                            "description": "Source columns that needs to be excluded from mapping"
                                        },
                                        "target_columns": {
                                            "type": "array",
                                            "description": "Target columns that needs to be excluded from mapping"
                                        }

                                    }
                                }
                            },
                            "required": [
                                "source_name"
                            ]
                        },
                        "description": "Source and target data mapping"
                    },
                    "execution_params": {
                        "type": "object",
                        "properties": {
                            "execute_on": {
                                "type": "string",
                                "description": "host to execute"
                            },
                            "memory_per_process": {
                                "type": "string",
                                "description": "memory per process"
                            },
                            "number_of_cores": {
                                "type": "number",
                                "description": "number of cores"
                            },
                            "number_of_process": {
                                "type": "number",
                                "description": "number of cores"
                            },
                            "parallel_flows": {
                                "type": "number",
                                "description": "number of flows that can be executed in parallel"
                            }
                        }
                    },
                    "grouping": {
                        "type": "boolean"
                    },
                    "grouping_params": {
                        "type": "object",
                        "properties": {
                            "large_table_limit": {
                                "type": "number",
                                "description": "large table limit"
                            },
                            "medium_table_limit": {
                                "type": "number",
                                "description": "medium table limit"
                            },
                            "small_table_limit": {
                                "type": "number",
                                "description": "small table limit"
                            }
                        }
                    }
                },
                "dependencies": {
                    "grouping_params": ["grouping"]
                },
                "required": ["name", "job_type", "mapping"]
            },
            "description": "List of jobs"
        }

    },
    "required": ["group", "project", "version", "source", "job"]
}