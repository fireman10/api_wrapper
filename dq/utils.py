import logging
import os
import requests
import json
import uuid
import re
import time




from datetime import datetime


class APIInvocationError(Exception):
    pass


class DQAPIExplorer(object):
    """dq flow executor"""

    def __init__(self, url: str, username: str, password: str, proj_group_name: str, project_name: str):
        """ dq api credentials
        :param url:str API url
        :param username:str user name
        :param password:str password
        """
        self.url = url
        self.username = username
        self.password = password
        self.proj_group_name = proj_group_name
        self.project_name = project_name
        self.__project_id = None
        self.__session_id = None
        self.auth_token = None

    @property
    def session_id(self) -> str:
        """ Session id getter"""
        self.__refresh_session_id()
        return self.__session_id

    @session_id.setter
    def session_id(self, value: str):
        """ Session id setter
        :param value:str session id
        """
        self.__session_id = value

    @property
    def project_id(self) -> str:
        """ DMS project ID getter"""
        self.__get_project_id()
        return self.__project_id

    @project_id.setter
    def project_id(self, value: str):
        """ DMS project ID setter
        :param value:str session id
        """
        self.__project_id= value

    def execute_flow(self, project_id_list: list, successful_emails: str = None, failure_emails: str = None,
                     parallel_flows=None, number_of_cores=None, number_of_process=None, job=None):
        """ Triggers dq flow
        :param project_id_list:str
        :param successful_emails:optional[str]
        :param failure_emails:optional[str]
        :param parallel_flows:optional[int]
        :param number_of_cores:optional[int]
        :param number_of_process:optional[int]
        :param job:optional[dic]
        :return: dict
        """
        if job and job.get('execution_params') is not None:

            execution_params = job.get('execution_params')
            parallel_flows = execution_params.get('parallel_flows')
            number_of_cores = execution_params.get('number_of_cores')
            number_of_process = execution_params.get('number_of_process')
            successful_emails = execution_params.get('successful_emails')
            failure_emails = execution_params.get('failure_emails')

        payload_params = {
                            "parallelJobExecutions": parallel_flows if parallel_flows else len(project_id_list),
                            "dbConnections": 1,
                            "flowExecutionConfigs": [{
                                "azkabanProjectId": project_id,
                                "successEmails": successful_emails,
                                "failureEmails": failure_emails,
                                "successEmailsOverride": 'true',
                                "failureEmailsOverride": 'true',
                                "groupId": self.proj_group_name,
                                "executors":number_of_process if number_of_process else 20,
                                "executorCores": number_of_cores if number_of_cores else 2,
                                "parameters": []
                                }for project_id in project_id_list
                            ]
                        }
        session_id = self.session_id
        headers = {"sessionid": session_id,
                   "session_id":session_id,
                   "Content-Type": 'application/json'
                   }

        try:
            logging.debug("executing flow")
            response = requests.post(f"{self.url}/dms/projects/{self.project_id}/batches/execute?",
                                     json=payload_params, headers=headers)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while triggering flow")

    def get_execution_status(self, execution_id: str):
        """ Returns result of flow execution
        :param execution_id:str
        :return: dict execution response
        """
        headers = {
            "sessionid": self.session_id,
        }
        try:
            logging.debug("getting execution result")
            response = requests.get(f'{self.url}/dms/executions/{execution_id}', headers=headers)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()

        except Exception:
            raise APIInvocationError("Error was raised while flow result execution")

    def get_result(self, execution_id: int):
        """ Returns result of flow execution
        :param execution_id:str
        :return: dict execution response
        """
        session_id = self.session_id
        auth_token = self.auth_token

        headers = {
            "sessionid": session_id,
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": auth_token
        }
        source_params = {
            "session.id": session_id,
            "session_id": session_id,
            "ajax": "fetchDVResult",
            "execid": execution_id,
            "action": "fetch"
        }
        source_params = "&".join([f"{k}={v}" for k, v in source_params.items() if v])
        try:
            logging.debug("getting execution result")
            response = requests.post(f'{self.url}/myapp/dvResults', json=source_params, headers=headers)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()

        except Exception:
            raise APIInvocationError("Error was raised while flow result execution")

    def get_table_info(self, connection_name, schema, table_name, is_schema_compare=False):
        session_id = self.session_id
        source_params = {
            "connectionName": connection_name,
            "currentUserGroup": self.proj_group_name,
            "databaseName": schema,
            "onlySchema": "true",
            "schema": schema,
            "session.id": session_id,
            "session_id": session_id,
            "tables": table_name,
            "indexedColumnDisabled": False if is_schema_compare else True,
            "foreignKeyDisabled": False if is_schema_compare else True
        }

        headers = {
            "sessionid": session_id,
            "session_id": session_id,
            "Content-Type": 'application/json'
        }

        try:
            logging.debug("getting table info")
            response = requests.post(f'{self.url}//dmsspark/connection/columns?', headers=headers,
                                     json=source_params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while getting table info")

    def get_table_schema(self,source, table_name):
        session_id = self.session_id
        source_params = {
            "connectionName": source['connection_name'],
            "currentUserGroup": self.proj_group_name,
            "databaseName": source['schema_name'],
            "onlySchema": "true",
            "schema": source['schema_name'],
            "session.id": session_id,
            "session_id": session_id,
            "tables": table_name,
        }
        headers = {
            "sessionid": session_id,
            "session_id": session_id,
            "Content-Type": 'application/json'
        }

        try:
            logging.debug("getting table table info")
            source_response = requests.post(f'{self.url}//dmsspark/connection/columns?', headers=headers,
                                            json=source_params)
            logging.debug(f"URL: {source_response.request.url}")
            logging.debug(f"Header: {source_response.request.headers}")
            logging.debug(f"Body: {source_response.request.body}")
            logging.debug(f"Response: {source_response.json()}")
            source_response = source_response.json()

            new_schema = []

            for schema in source_response[table_name]['schema']:
                new_schema.append(
                    {
                        "dataType": schema['columnType'],
                        "name": schema['columnName'],
                        "nullable": schema['nullable']
                    }
                )
            return new_schema
        except Exception:
            raise APIInvocationError("Error was raised while getting source schema")

    def get_sql_info(self, connection_name, schema, sql_query):
        session_id = self.session_id
        params = {
            "session.id": session_id,
            "session_id": session_id,
            "ajax": "get",
            "action": "sqlQuerySchema",
            "connectionName": connection_name,
            "dataBaseName": schema,
            "sqlQuery": sql_query
        }
        try:
            logging.debug("getting sql info")
            response = requests.post(f'{self.url}/myapp/JDBCDeatilsServlet', data=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while getting table info")

    def get_column_mapping(self, source: dict, target: dict, element: set, source_exclude_column: list,
                           target_exclude_column):
        session_id = self.session_id
        columns_mapping = []
        source_column = []
        target_column = []
        if source['source_type'] == 'table':
            source_params = {
                    "connectionName": source['connection_name'],
                    "currentUserGroup": self.proj_group_name,
                    "databaseName": source['schema_name'],
                    "onlySchema": "true",
                    "schema": source['schema_name'],
                    "session.id": session_id,
                    "session_id": session_id,
                    "tables": element[0],
            }
            headers = {
                "sessionid": session_id,
                "session_id": session_id,
                "Content-Type": 'application/json'
            }

            try:
                logging.debug("Getting source columns")
                response = requests.post(f'{self.url}//dmsspark/connection/columns?', headers=headers,
                                                json=source_params)
                logging.debug(f"URL: {response.request.url}")
                logging.debug(f"Header: {response.request.headers}")
                logging.debug(f"Body: {response.request.body}")
                logging.debug(f"Response: {response.json()}")
                source_response = response.json()
                source_column = [schema['columnName'] for schema in source_response[element[0]]['schema']]
            except Exception:
                raise APIInvocationError("Error was raised while getting source table columns")

        elif source['source_type'] == 'sql':
            source_params = {
                "connectionName": source['connection_name'],
                "currentUserGroup": self.proj_group_name,
                "databaseName": source['schema_name'],
                "session.id": session_id,
                "session_id": session_id,
                "sqlQuery": element[0],
            }
            headers = {
                "sessionid": session_id,
                "session_id": session_id,
                "Content-Type": 'application/json'
            }
            try:
                logging.debug("Getting source columns")
                response = requests.post(f'{self.url}/dmsspark/connection/sqlQuerySchema',
                                                headers=headers,
                                                json=source_params)
                logging.debug(f"URL: {response.request.url}")
                logging.debug(f"Header: {response.request.headers}")
                logging.debug(f"Body: {response.request.body}")
                logging.debug(f"Response: {response.json()}")
                source_response = response.json()
                source_column = [schema['columnName'] for schema in source_response]
            except Exception:
                raise APIInvocationError("Error was raised while getting source sql columns")
        elif source['source_type'] == 'file':

            headers = {
                "sessionid": session_id,
                "session_id": session_id,
                "Content-Type": 'application/json'
            }
            source_params = {}
            if source['source_format'] == 'CSV':
                source_params = {
                    "filePath": source['source_path'],
                    "delimiter": source['delimiter'],
                    "inferSchema": "true" if source['inferSchema'] else "false",
                    "header": "true" if source['header'] else "false",
                    "sampleDataFlag": "true",
                    "fileUploadType": 's3' if re.match("^s3", source['source_path']) else '',
                    "columns": [],
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }

            elif source['source_format'] == 'PARQUET':
                source_params = {
                    "filePath": source['source_path'],
                    "sampleDataFlag": "true",
                    "fileUploadType": 's3' if re.match("^s3", source['source_path']) else '',
                    "columns": [],
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }
            elif source['source_format'] == 'JSON':
                source_params = {
                    "filePath": source['source_path'],
                    "multiline": source['multiline'],
                    "flattenJson": source.get('flatten_data',''),
                    "sampleDataFlag": "true",
                    "fileUploadType": 's3' if re.match("^s3", source['source_path']) else '',
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }
            elif source['source_format'] == 'EXCEL':
                source_params = {
                    "inferSchema": source['inferSchema'],
                    "header": source['header'],
                    "sheetNames": [],
                    "fileUploadType": 's3' if re.match("^s3", source['source_path']) else '',
                    "filePath": source['source_path'],
                    "sampleDataFlag": "true",
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }

            try:
                logging.debug("Getting source columns")
                source_response = requests.post(f'{self.url}/dmsspark/file/parser/{source["source_format"].lower()}?',
                                                headers=headers, json=source_params)
                logging.debug(f"URL: {source_response.request.url}")
                logging.debug(f"Header: {source_response.request.headers}")
                logging.debug(f"Body: {source_response.request.body}")
                logging.debug(f"Response: {source_response.json()}")

                source_column = [schema['columnName'] for schema in json.loads(source_response.json()['Schema'])]

            except Exception:
                raise APIInvocationError("Error was raised while getting source CSV file columns")

        if target['target_type'] == 'table':
            target_params = {
                    "connectionName": target['connection_name'],
                    "currentUserGroup": self.proj_group_name,
                    "databaseName": target['schema_name'],
                    "onlySchema": "true",
                    "schema": target['schema_name'],
                    "session.id": session_id,
                    "session_id": session_id,
                    "tables": element[1],
            }
            headers = {
                "sessionid": session_id,
                "session_id": session_id,
                "Content-Type": 'application/json'
            }
            try:
                logging.debug("Getting target columns")
                response = requests.post(f'{self.url}/dmsspark/connection/columns?',
                                                headers=headers, json=target_params)
                logging.debug(f"URL: {response.request.url}")
                logging.debug(f"Header: {response.request.headers}")
                logging.debug(f"Body: {response.request.body}")
                logging.debug(f"Response: {response.json()}")
                target_response = response.json()
                target_column = [schema['columnName'] for schema in target_response[element[1]]['schema']]

            except Exception:
                raise APIInvocationError("Error was raised while getting target table columns")

        elif target['target_type'] == 'sql':
            target_params = {
                "connectionName": source['connection_name'],
                "currentUserGroup": self.proj_group_name,
                "databaseName": source['schema_name'],
                "session.id": session_id,
                "session_id": session_id,
                "sqlQuery": element[0],
            }
            headers = {
                "sessionid": session_id,
                "session_id": session_id,
                "Content-Type": 'application/json'
            }
            try:
                response = requests.post(f'{self.url}//dmsspark/connection/sqlQuerySchema?',
                                                headers=headers,
                                                json=target_params)
                logging.debug(f"URL: {response.request.url}")
                logging.debug(f"Header: {response.request.headers}")
                logging.debug(f"Body: {response.request.body}")
                logging.debug(f"Response: {response.json()}")
                target_response = response.json()
                target_column = [schema['columnName'] for schema in target_response]
            except Exception:
                raise APIInvocationError("Error was raised while getting target sql columns")
        elif target['target_type'] == 'file':

            headers = {
                "sessionid": session_id,
                "session_id": session_id,
                "Content-Type": 'application/json'
            }
            target_params = {}
            if target['target_format'] == 'CSV':
                target_params = {
                    "filePath": target['target_path'],
                    "delimiter": target['delimiter'],
                    "inferSchema": "true" if target['inferSchema'] else "false",
                    "header": "true" if target['header'] else "false",
                    "columns": [],
                    "sampleDataFlag": "true",
                    "fileUploadType": 's3' if re.match("^s3", target['target_path']) else '',
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }
            elif target['target_format'] == 'PARQUET':
                target_params = {
                    "filePath": target['target_path'],
                    "sampleDataFlag": "true",
                    "fileUploadType": 's3' if re.match("^s3", target['target_path']) else '',
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }
            elif target['target_format'] == 'JSON':
                target_params = {
                    "filePath": target['target_path'],
                    "multiline": target['multiline'],
                    "flattenJson": target.get('flatten_data',False),
                    "sampleDataFlag": "true",
                    "fileUploadType": 's3' if re.match("^s3", target['target_path']) else '',
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }
            elif target['target_format'] == 'EXCEL':
                target_params = {
                    "inferSchema": target['inferSchema'],
                    "header": target['header'],
                    "sheetNames": [],
                    "fileUploadType": 's3' if re.match("^s3", target['target_path']) else '',
                    "filePath": target['target_path'],
                    "sampleDataFlag": "true",
                    "session.id": session_id,
                    "session_id": session_id,
                    "executeOn": "LOCAL",
                    "userGroup": self.proj_group_name
                }
            try:
                response = requests.post(f'{self.url}/dmsspark/file/parser/{target["target_format"].lower()}?',
                                                headers=headers,
                                                json=target_params)
                logging.debug(f"URL: {response.request.url}")
                logging.debug(f"Header: {response.request.headers}")
                logging.debug(f"Body: {response.request.body}")
                logging.debug(f"Response: {response.json()}")
                target_response = response.json()
                target_column = [schema['columnName'] for schema in json.loads(target_response['Schema'])]

            except Exception:
                raise APIInvocationError("Error was raised while getting source sql columns")

        for exclude_column in source_exclude_column:
            try:
                source_column.remove(exclude_column)
            except:
                pass
        for exclude_column in target_exclude_column:
            try:
                target_column.remove(exclude_column)
            except:
                pass
        target_column_upper = [element.upper() for element in target_column]
        for source_column_element in source_column:
            if source_column_element.upper() in target_column_upper:
                columns_mapping.append((source_column_element,
                                        target_column[target_column_upper.index(source_column_element.upper())]))
        return columns_mapping

    def create_compare_flow(self, mapping: dict, sources: dict, targets: dict, job: dict, flow_des_suffix: str):

        session_id = self.session_id
        default_number_of_cores = 2
        default_memory_per_process = '8g'
        default_execute_on = 'LOCAL'
        default_number_of_process = 20
        flow_name = f"dq{datetime.now().strftime('%Y%m%d%H%M%S%f')}"
        flow_desc = job['name'] if flow_des_suffix == '' else f"{job['name']}_{flow_des_suffix}"

        source_data = []
        for source in sources:
            if source['source_type'] == 'sql':
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in source['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in source['data_set']],
                            "transformationSQL": True
                        })
            elif source['source_type'] == 'file':
                if source['source_format'] == 'CSV':
                    source_data.append({
                        "columns": [],
                        "datasetDelimiter": source['delimiter'],
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'JSON':
                    source_data.append({
                        "columns": source.get('columns', []),
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "flattenJson": source.get('flatten_data',False),
                        "multiLine": source['multiline'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'PARQUET':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'EXCEL':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })

            else:
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": source['source_table_properties']
                        })

            target_data = []
            for target in targets:
                if target['target_type'] == 'sql':
                    target_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": target['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": target['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in target['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in target['data_set']],
                            "transformationSQL": True
                        })
                elif target['target_type'] == 'file':
                    if target['target_format'] == 'CSV':
                        target_data.append({
                            "columns": [],
                            "datasetDelimiter": target['delimiter'],
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "header": target['header'],
                            "inferSchema": target['inferSchema'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'JSON':
                        target_data.append({
                            "columns": target.get('columns', []),
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "flattenJson": target.get('flatten_data', False),
                            "multiLine": target['multiline'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'PARQUET':
                        target_data.append({
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'EXCEL':
                        target_data.append({
                            "datasetFormat": target['target_format'],
                            "header": target['header'],
                            "inferSchema": target['inferSchema'],
                            "datasetPath": target['target_path'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })

                else:
                    target_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": target['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": target['connection_name']},
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": target['target_table_properties']
                        } )



        params = {
            "executorCores": job['execution_params']['number_of_cores'] if 'execution_params' in job.keys() and
                             'number_of_cores' in job['execution_params'].keys() else default_number_of_cores,
            "executorMemory": job['execution_params']['memory_per_process'] if 'execution_params' in job.keys() and
                             'memory_per_process' in job['execution_params'].keys() else default_memory_per_process,
            "email": [],
            "flowName": flow_name,
            "flowDesc": flow_desc,
            "hadoop": False,
            "livy": False,
            "numExecutors": job['execution_params']['number_of_process'] if 'execution_params' in job.keys() and
                             'number_of_process' in job['execution_params'].keys() else default_number_of_process,
            "root": "component2",
            "executeOn": job['execution_params']['execute_on'].upper() if 'execution_params' in job.keys() and
                             'execute_on' in job['execution_params'].keys() else default_execute_on,
            "API_APP_BUILD": "Jun 5, 2021 11:37:08 PM",
            "UI_VERSION": "176-4c84765dc5",
            "APP_VERSION": "589-2caf3fb81d",
            "LicensedName": "microsoft",
            "flowType": "DATA MIGRATION",
            "jobMap": {
                "component0": {
                    "id": "component0",
                    "type": "InputSource",
                    "isLeft": True,
                    "dependentJob": True,
                    "description": "Source",
                    "position": {"x": 141, "y": 147},
                    "sourceData": source_data
                },
                "component1": {
                    "id": "component1",
                    "type": "InputSource",
                    "dependentJob": True,
                    "description": "Target",
                    "position": {"x": 139, "y": 314},
                    "sourceData": target_data
                },
                "component2": {
                    "id": "component2",
                    "type": "DataCompare",
                    "dependentJob": True,
                    "position": {"x": 428, "y": 197},
                    "description": "Target",
                    "dependsOn": ["component0", "component1"],
                    "jobType": "CELL_BY_CELL_COMPARE",
                    "filesCompareList": [
                        {
                            "compareType": "CELL_BY_CELL_COMPARE",
                            "compareColumns": False,
                            "compareCommonColumnsOnly": True,
                            "srcDestTableMap": {
                                element['srcDestTableMap'][0]: element['srcDestTableMap'][1]
                            },
                            "srcDestComponentMap": {
                                "component0": "component1"
                            },
                            "saveAllDiffOutput": "",
                            "validateRowsCount": False,
                            "tolerance": [],
                            "sqlTransform": [],
                            "compareKey": self.get_compare_key_mapping(element['compareKeyNew'][0], element['compareKeyNew'][1])[1],
                            "compareKeyNew": self.get_compare_key_mapping(element['compareKeyNew'][0], element['compareKeyNew'][1])[0],
                            "columnMapping": [
                                {
                                    "srcColumn": column_mapping_element[0],
                                    "destColumn": column_mapping_element[1]
                                }for column_mapping_element in element['columnMapping']
                            ],
                            "matchBoth": True,
                            "keyValueProperties": {
                                "DATA_MIGRATION": "true"
                            }
                        }
                        for element in mapping
                    ]
                }
            }
        }


        for index in range(0, len(params['jobMap']['component2']['filesCompareList'])):
            if not params['jobMap']['component2']['filesCompareList'][index]['compareKey']:
                del params['jobMap']['component2']['filesCompareList'][index]['compareKey']
                del params['jobMap']['component2']['filesCompareList'][index]['compareKeyNew']

        url_parms = {
            "dmsProjectId": self.project_id,
            "project_name": flow_name,
            "project_description": flow_desc,
            "session_id": session_id,
            "session.id": session_id,

        }
        params = json.dumps(params)
        params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])

        try:
            logging.debug("create compare flow")
            response = requests.post(f"{self.url}/myapp/dvFlowUpload?{params_str}", data=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while creating new compare flow")

    def create_schema_compare_flow(self, mapping: dict, sources: dict, targets: dict, job: dict, flow_des_suffix: str):

        session_id = self.session_id
        default_number_of_cores = 2
        default_memory_per_process = '8g'
        default_execute_on = 'LOCAL'
        default_number_of_process = 20
        flow_name = f"dq{datetime.now().strftime('%Y%m%d%H%M%S%f')}"
        flow_desc = job['name'] if flow_des_suffix == '' else f"{job['name']}_{flow_des_suffix}"

        source_data = []
        for source in sources:
            if source['source_type'] == 'sql':
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in source['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in source['data_set']],
                            "transformationSQL": True
                        })
            elif source['source_type'] == 'file':
                if source['source_format'] == 'CSV':
                    source_data.append({
                        "columns": [],
                        "datasetDelimiter": source['delimiter'],
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'JSON':
                    source_data.append({
                        "columns": source.get('columns', []),
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "flattenJson": source.get('flatten_data',False),
                        "multiLine": source['multiline'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'PARQUET':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'EXCEL':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })

            else:
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": source['source_table_properties']
                        })

            target_data = []
            for target in targets:
                if target['target_type'] == 'sql':
                    target_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": target['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": target['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in target['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in target['data_set']],
                            "transformationSQL": True
                        })
                elif target['target_type'] == 'file':
                    if target['target_format'] == 'CSV':
                        target_data.append({
                            "columns": [],
                            "datasetDelimiter": target['delimiter'],
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "header": target['header'],
                            "inferSchema": target['inferSchema'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'JSON':
                        target_data.append({
                            "columns": target.get('columns', []),
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "flattenJson": target.get('flatten_data', False),
                            "multiLine": target['multiline'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'PARQUET':
                        target_data.append({
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'EXCEL':
                        target_data.append({
                            "datasetFormat": target['target_format'],
                            "header": target['header'],
                            "inferSchema": target['inferSchema'],
                            "datasetPath": target['target_path'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })

                else:
                    target_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": target['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": target['connection_name']},
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": target['target_table_properties']
                        } )



        params = {
            "executorCores": job['execution_params']['number_of_cores'] if 'execution_params' in job.keys() and
                             'number_of_cores' in job['execution_params'].keys() else default_number_of_cores,
            "executorMemory": job['execution_params']['memory_per_process'] if 'execution_params' in job.keys() and
                             'memory_per_process' in job['execution_params'].keys() else default_memory_per_process,
            "email": [],
            "flowName": flow_name,
            "flowDesc": flow_desc,
            "hadoop": False,
            "livy": False,
            "numExecutors": job['execution_params']['number_of_process'] if 'execution_params' in job.keys() and
                             'number_of_process' in job['execution_params'].keys() else default_number_of_process,
            "root": "component2",
            "executeOn": job['execution_params']['execute_on'].upper() if 'execution_params' in job.keys() and
                             'execute_on' in job['execution_params'].keys() else default_execute_on,
            "API_APP_BUILD": "Jun 5, 2021 11:37:08 PM",
            "UI_VERSION": "176-4c84765dc5",
            "APP_VERSION": "589-2caf3fb81d",
            "LicensedName": "microsoft",
            "flowType": "SCHEMA COMPARE",
            "jobMap": {
                "component0": {
                    "id": "component0",
                    "type": "InputSource",
                    "isLeft": True,
                    "dependentJob": True,
                    "description": "Source",
                    "position": {"x": 141, "y": 147},
                    "sourceData": source_data
                },
                "component1": {
                    "id": "component1",
                    "type": "InputSource",
                    "dependentJob": True,
                    "description": "Target",
                    "position": {"x": 139, "y": 314},
                    "sourceData": target_data
                },
                "component2": {
                    "id": "component2",
                    "type": "DataCompare",
                    "dependentJob": True,
                    "position": {"x": 428, "y": 197},
                    "description": "Target",
                    "dependsOn": ["component0", "component1"],
                    "jobType": "SCHEMA_COMPARE",
                    "filesCompareList": [
                        {
                            "compareType": "SCHEMA_COMPARE",
                            "compareColumns": False,
                            "compareCommonColumnsOnly": True,
                            "srcDestTableMap": {
                                element['srcDestTableMap'][0]: element['srcDestTableMap'][1]
                            },
                            "srcDestComponentMap": {
                                "component0": "component1"
                            },
                            "saveAllDiffOutput": "",
                            "validateRowsCount": False,
                            "tolerance": [],
                            "sqlTransform": [],
                            "compareKeyNew": self.get_compare_key_mapping(element['compareKeyNew'][0], element['compareKeyNew'][1])[0],
                            "columnMapping": [
                                {
                                    "srcColumn": column_mapping_element[0],
                                    "destColumn": column_mapping_element[1]
                                }for column_mapping_element in element['columnMapping']
                            ]

                        }
                        for element in mapping
                    ]
                }
            }
        }


        for index in range(0, len(params['jobMap']['component2']['filesCompareList'])):
            if not params['jobMap']['component2']['filesCompareList'][index]['compareKeyNew']:
                del params['jobMap']['component2']['filesCompareList'][index]['compareKeyNew']

        url_parms = {
            "dmsProjectId": self.project_id,
            "project_name": flow_name,
            "project_description": flow_desc,
            "session_id": session_id,
            "session.id": session_id,

        }
        params = json.dumps(params)
        params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])

        try:
            logging.debug("create compare flow")
            response = requests.post(f"{self.url}/myapp/dvFlowUpload?{params_str}", data=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while creating new compare flow")

    def get_compare_key_mapping(self, source_compare_key, target_compare_key):
        mapping_list = []
        source_compare_list = []
        for source in source_compare_key:
            for target in target_compare_key:
                if target.casefold() == source.casefold():
                    mapping_list.append({"srcColumn": source, "destColumn": target})
                    source_compare_list.append(source)

        return mapping_list, source_compare_list

    def create_etl_flow(self, mapping: dict, sources: dict, targets: dict, job: dict, flow_des_suffix: str):

        session_id = self.session_id
        default_number_of_cores = 2
        default_memory_per_process = '8g'
        default_execute_on = 'LOCAL'
        default_number_of_process = 20
        flow_name = f"dq{datetime.now().strftime('%Y%m%d%H%M%S%f')}"
        flow_desc = job['name'] if flow_des_suffix == '' else f"{job['name']}_{flow_des_suffix}"

        source_data = []
        for source in sources:
            if source['source_type'] == 'sql':
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in source['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in source['data_set']],
                            "transformationSQL": True
                        })
            elif source['source_type'] == 'file':
                if source['source_format'] == 'CSV':
                    source_data.append({
                        "columns": [],
                        "datasetDelimiter": source['delimiter'],
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'JSON':
                    source_data.append({
                        "columns": source.get('columns', []),
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "flattenJson": source.get('flatten_data',False),
                        "multiLine": source['multiline'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'PARQUET':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'EXCEL':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })

            else:
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "tableNamesList": source['source_data_set_properties'],
                            "tableProperties": source['source_table_properties']
                        })

            target_data = []
            for target in targets:
                if target['target_type'] == 'sql':
                    target_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": target['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": target['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in target['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in target['data_set']],
                            "transformationSQL": True
                        })
                elif target['target_type'] == 'file':
                    if target['target_format'] == 'CSV':
                        target_data.append({
                            "columns": [],
                            "datasetDelimiter": target['delimiter'],
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "header": target['header'],
                            "inferSchema": target['inferSchema'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'JSON':
                        target_data.append({
                            "columns": target.get('columns', []),
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "flattenJson": target.get('flatten_data', False),
                            "multiLine": target['multiline'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'PARQUET':
                        target_data.append({
                            "datasetFormat": target['target_format'],
                            "datasetPath": target['target_path'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })
                    elif target['target_format'] == 'EXCEL':
                        target_data.append({
                            "datasetFormat": target['target_format'],
                            "header": target['header'],
                            "inferSchema": target['inferSchema'],
                            "datasetPath": target['target_path'],
                            "tableNamesList": {data_set['name']: None for data_set in target['data_set']},
                            "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                                for data_set in target['data_set']]
                        })

                else:
                    target_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": target['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": target['connection_name']},
                            "tableNamesList": target['target_data_set_properties'],
                            "tableProperties": target['target_table_properties']
                        } )



        params = {
            "executorCores": job['execution_params']['number_of_cores'] if 'execution_params' in job.keys() and
                             'number_of_cores' in job['execution_params'].keys() else default_number_of_cores,
            "executorMemory": job['execution_params']['memory_per_process'] if 'execution_params' in job.keys() and
                             'memory_per_process' in job['execution_params'].keys() else default_memory_per_process,
            "email": [],
            "flowName": flow_name,
            "flowDesc": flow_desc,
            "hadoop": False,
            "livy": False,
            "numExecutors": job['execution_params']['number_of_process'] if 'execution_params' in job.keys() and
                             'number_of_process' in job['execution_params'].keys() else default_number_of_process,
            "root": "component2",
            "executeOn": job['execution_params']['execute_on'].upper() if 'execution_params' in job.keys() and
                             'execute_on' in job['execution_params'].keys() else default_execute_on,
            "API_APP_BUILD": "Jun 5, 2021 11:37:08 PM",
            "UI_VERSION": "176-4c84765dc5",
            "APP_VERSION": "589-2caf3fb81d",
            "LicensedName": "microsoft",
            "flowType": "CELL BY CELL COMPARE",
            "jobMap": {
                "component0": {
                    "id": "component0",
                    "type": "InputSource",
                    "isLeft": True,
                    "dependentJob": True,
                    "description": "Source",
                    "position": {"x": 141, "y": 147},
                    "sourceData": source_data
                },
                "component1": {
                    "id": "component1",
                    "type": "InputSource",
                    "dependentJob": True,
                    "description": "Target",
                    "position": {"x": 139, "y": 314},
                    "sourceData": target_data
                },
                "component2": {
                    "id": "component2",
                    "type": "DataCompare",
                    "dependentJob": True,
                    "position": {"x": 428, "y": 197},
                    "description": "Target",
                    "dependsOn": ["component0", "component1"],
                    "jobType": "CELL_BY_CELL_COMPARE",
                    "filesCompareList": [
                        {
                            "compareType": "CELL_BY_CELL_COMPARE",
                            "compareColumns": False,
                            "compareCommonColumnsOnly": True,
                            "srcDestTableMap": {
                                element['srcDestTableMap'][0]: element['srcDestTableMap'][1]
                            },
                            "srcDestComponentMap": {
                                "component0": "component1"
                            },
                            "saveAllDiffOutput": "",
                            "validateRowsCount": False,
                            "tolerance": [],
                            "sqlTransform": [],
                            "compareKey": self.get_compare_key_mapping(element['compareKeyNew'][0], element['compareKeyNew'][1])[1],
                            "compareKeyNew": self.get_compare_key_mapping(element['compareKeyNew'][0], element['compareKeyNew'][1])[0],
                            "columnMapping": [
                                {
                                    "srcColumn": column_mapping_element[0],
                                    "destColumn": column_mapping_element[1]
                                }for column_mapping_element in element['columnMapping']
                            ],
                            "matchBoth": True,
                        }
                        for element in mapping
                    ]
                }
            }
        }
        for index in range(0, len(params['jobMap']['component2']['filesCompareList'])):
            if not params['jobMap']['component2']['filesCompareList'][index]['compareKey']:
                del params['jobMap']['component2']['filesCompareList'][index]['compareKey']
                del params['jobMap']['component2']['filesCompareList'][index]['compareKeyNew']

        url_parms = {
            "dmsProjectId": self.project_id,
            "project_name": flow_name,
            "project_description": flow_desc,
            "session_id": session_id,
            "session.id": session_id,

        }
        params = json.dumps(params)
        params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])

        try:
            logging.debug("creating ETL flow")
            response = requests.post(f"{self.url}/myapp/dvFlowUpload?{params_str}", data=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while creating new compare flow")

    def get_compare_key_mapping(self, source_compare_key, target_compare_key):
        mapping_list = []
        source_compare_list = []
        for source in source_compare_key:
            for target in target_compare_key:
                if target.casefold() == source.casefold():
                    mapping_list.append({"srcColumn": source, "destColumn": target})
                    source_compare_list.append(source)

        return mapping_list, source_compare_list


    def get_tables(self,source):
        session_id = self.session_id
        source_params = {
            "connectionName": source['connection_name'],
            "currentUserGroup": self.proj_group_name,
            "databaseName": source['schema_name'],
            "schema": source['schema_name'],
            "session.id": session_id,
            "session_id": session_id,
        }
        headers = {
            "sessionid": session_id,
            "session_id": session_id,
            "Content-Type": 'application/json'
        }

        try:
            logging.debug("getting tables")
            source_response = requests.post(f'{self.url}//dmsspark/connection/tables', headers=headers,
                                            json=source_params)
            logging.debug(f"URL: {source_response.request.url}")
            logging.debug(f"Header: {source_response.request.headers}")
            logging.debug(f"Body: {source_response.request.body}")
            logging.debug(f"Response: {source_response.json()}")
            source_response = source_response.json()

            return source_response
        except Exception:
            raise APIInvocationError("Error was raised while getting source schema")

    def create_data_profile_flow(self, sources: dict, job: dict):
        session_id = self.session_id
        default_number_of_cores = 2
        default_memory_per_process = '8g'
        default_execute_on = 'LOCAL'
        default_number_of_process = 20
        flow_name = f"dq{datetime.now().strftime('%Y%m%d%H%M%S%f')}"

        source_data = []
        for source in sources:
            if source['source_type'] == 'sql':
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in source['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in source['data_set']],
                            "transformationSQL": True
                        })
            elif source['source_type'] == 'file':
                if source['source_format'] == 'CSV':
                    source_data.append({
                        "columns": [],
                        "datasetDelimiter": source['delimiter'],
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'JSON':
                    source_data.append({
                        "columns": source.get('columns', []),
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "flattenJson": source.get('flatten_data',False),
                        "multiLine": source['multiline'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'PARQUET':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'EXCEL':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })

            else:
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": source['source_table_properties']
                        })
        flow_desc = job['name']
        params = {
            "executorCores": job['execution_params']['number_of_cores'] if 'execution_params' in job.keys() and
                             'number_of_cores' in job['execution_params'].keys() else default_number_of_cores,
            "executorMemory": job['execution_params']['memory_per_process'] if 'execution_params' in job.keys() and
                             'memory_per_process' in job['execution_params'].keys() else default_memory_per_process,
            "email": [],
            "flowName": flow_name,
            "flowDesc": flow_desc,
            "hadoop": False,
            "livy": False,
            "numExecutors": job['execution_params']['number_of_process'] if 'execution_params' in job.keys() and
                             'number_of_process' in job['execution_params'].keys() else default_number_of_process,
            "root": "component0",
            "executeOn": job['execution_params']['execute_on'].upper() if 'execution_params' in job.keys() and
                             'execute_on' in job['execution_params'].keys() else default_execute_on,
            "API_APP_BUILD": "Jun 5, 2021 11:37:08 PM",
            "UI_VERSION": "176-4c84765dc5",
            "APP_VERSION": "589-2caf3fb81d",
            "LicensedName": "microsoft",
            "flowType": "DATA PROFILE",
            "jobMap": {
                "component0": {
                    "id": "component0",
                    "dependsOn": [
                        "component1"
                    ],
                    "isLeft": True,
                    "dependentJob": True,
                    "type": "DataProfile",
                    "jobType": "DATA_PROFILE",
                    "description": "data_profile_job",
                    "position": {"x": 141, "y": 147}
                },
                "component1": {
                    "id": "component1",
                    "type": "InputSource",
                    "dependentJob": True,
                    "description": "Target",
                    "position": {"x": 139, "y": 314},
                    "sourceData": source_data
                }
            }
        }

        url_parms = {
            "dmsProjectId": self.project_id,
            "project_name": flow_name,
            "project_description": flow_desc,
            "session_id": session_id,
            "session.id": session_id,

        }
        params = json.dumps(params)
        params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])

        try:
            logging.debug("creating data profile flow")
            response = requests.post(f"{self.url}/myapp/dvFlowUpload?{params_str}", data=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while creating new compare flow")

    def create_data_quality_flow(self, sources: dict, job: dict):
        session_id = self.session_id
        default_number_of_cores = 2
        default_memory_per_process = '8g'
        default_execute_on = 'LOCAL'
        default_number_of_process = 20
        flow_name = f"dq{datetime.now().strftime('%Y%m%d%H%M%S%f')}"

        source_data = []
        for source in sources:
            if source['source_type'] == 'sql':
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "sqlStatements": {data_set['name']: [{"sql": data_set['query'], "name": data_set['name']}]
                                              for data_set in source['data_set']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": [{"tblName": data_set['name']}for data_set in source['data_set']],
                            "transformationSQL": True
                        })
            elif source['source_type'] == 'file':
                if source['source_format'] == 'CSV':
                    source_data.append({
                        "columns": [],
                        "datasetDelimiter": source['delimiter'],
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'JSON':
                    source_data.append({
                        "columns": source.get('columns', []),
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "flattenJson": source.get('flatten_data',False),
                        "multiLine": source['multiline'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'PARQUET':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })
                elif source['source_format'] == 'EXCEL':
                    source_data.append({
                        "datasetFormat": source['source_format'],
                        "header": source['header'],
                        "inferSchema": source['inferSchema'],
                        "datasetPath": source['source_path'],
                        "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                        "tableProperties": [{"tblName": data_set['name'], "tblRecordCount": 0, "tblSize": "0"}
                                            for data_set in source['data_set']]
                    })

            else:
                source_data.append({
                            "datasetFormat": "JDBC",
                            "databaseName": source['schema_name'],
                            "datasetPath": "projects",
                            "jdbcData": {"connectionName": source['connection_name']},
                            "tableNamesList": {data_set['name']: None for data_set in source['data_set']},
                            "tableProperties": source['source_table_properties']
                        })

        edit_data = {}

        # column rules
        dq_rules = {}
        if 'column_rules' in job.keys():
            for column_rule in job['column_rules']:
                dq_rules[column_rule['source_data_set']] ={
                                                            "rules": []
                                                           }

                for rule in column_rule['rules']:
                    time.sleep(1)
                    if 'empty_check' in rule.keys() and rule['empty_check']:
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "NOT_EMPTY",
                                "operator": "=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'null_check' in rule.keys():
                        if 'operator' not in rule['null_check'].keys() or 'expected_percent' not in rule['null_check'].keys():
                            raise APIInvocationError("operator or expected percentage key is not present for null check")
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "NOT_NULLS",
                                "column": rule['column'],
                                "operator": rule['null_check']['operator'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning",
                                "expectedPercent": rule['null_check']['expected_percent'],
                                "description": "Unique"
                            }
                        )
                    time.sleep(1)
                    if 'unique_check' in rule.keys():
                        if 'operator' not in rule['unique_check'].keys() \
                                or 'expected_percent' not in rule['unique_check'].keys():
                            raise APIInvocationError("operator or expected percentage key is not present for null check")
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "UNIQUE",
                                "column": rule['column'],
                                "operator": rule['unique_check']['operator'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning",
                                "expectedPercent": rule['unique_check']['expected_percent'],
                                "description": "Unique"
                            }
                        )
                    time.sleep(1)
                    if 'left_spaces' in rule.keys() and rule['left_spaces']:
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "LEFT_SPACES",
                                "operator": "=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'right_spaces' in rule.keys() and rule['right_spaces']:
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "RIGHT_SPACES",
                                "operator": "=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'min_length' in rule.keys():
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "MIN_LENGTH",
                                "operator": ">=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "ruleValue": rule['min_length'],
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'max_length' in rule.keys():
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "MAX_LENGTH",
                                "operator": "<=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "ruleValue": rule['max_length'],
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'min_value' in rule.keys():
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "MIN_VALUE",
                                "operator": ">=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "ruleValue": rule['min_value'],
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'max_value' in rule.keys():
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "MAX_VALUE",
                                "operator": ">=",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "ruleValue": rule['max_value'],
                                "level": "Warning"
                            }
                        )
                    time.sleep(1)
                    if 'sql' in rule.keys():
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "SQL_WHERE",
                                "column": rule['column'],
                                "operator": "=",
                                "sql": rule['sql'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning",

                            }
                        )
                    time.sleep(1)
                    if 'regular_expression' in rule.keys():
                        dq_rules[column_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "REGULAR_EXPRESSION",
                                "column": rule['column'],
                                "creationTime": int(time.time()*1000),
                                "regularExpression": rule['regular_expression'],
                                "level": "Warning"
                            }
                        )


        # sql rules
        if 'sql_rules' in job.keys():
            for sql_rule in job['sql_rules']:
                if sql_rule['source_data_set'] not in dq_rules.keys():
                    dq_rules[sql_rule['source_data_set']] = {
                        "rules": []
                    }
                if 'source_data_set' not in sql_rule.keys() or 'rules' not in sql_rule.keys():
                                    raise APIInvocationError("source_data_set or rules  key is not present for sql_rules")

                else:

                    for rule in sql_rule['rules']:
                        time.sleep(1)
                        dq_rules[sql_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "SQL",
                                "column": "N/A",
                                "sql": rule['query'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning",
                                "name": rule['name']
                            }
                        )

        # table metadata rules
        if 'table_metadata_rules' in job.keys():
            for table_metadata_rule in job['table_metadata_rules']:
                if table_metadata_rule['source_data_set'] not in dq_rules.keys():
                    dq_rules[table_metadata_rule['source_data_set']] = {
                        "rules": []
                    }
                if 'source_data_set' not in table_metadata_rule.keys():
                    raise APIInvocationError("source_data_set key is not present in table_metadata_rule")
                time.sleep(1)
                if 'minimum_records_count' in table_metadata_rule.keys():
                    dq_rules[table_metadata_rule['source_data_set']]['rules'].append(
                        {
                            "tblMetadataInfo": {
                                "count": 50000
                            },
                            "ruleType": "RECORD_COUNT_MIN",
                            "operator": "=",
                            "tblName": table_metadata_rule['source_data_set'],
                            "creationTime": int(time.time()*1000),
                            "ruleValue": table_metadata_rule['minimum_records_count'],
                            "level": "Warning"
                        }
                        )
                time.sleep(1)
                if 'maximum_records_count' in table_metadata_rule.keys():
                    dq_rules[table_metadata_rule['source_data_set']]['rules'].append(
                        {
                            "tblMetadataInfo": {
                                "count": 50000
                            },
                            "ruleType": "RECORD_COUNT_MAX",
                            "operator": "=",
                            "tblName": table_metadata_rule['source_data_set'],
                            "creationTime": int(time.time()*1000),
                            "ruleValue": table_metadata_rule['maximum_records_count'],
                            "level": "Warning"
                        }
                        )
                time.sleep(1)
                if 'columns_name' in table_metadata_rule.keys() and table_metadata_rule['columns_name']:
                    this_source = None
                    for source in sources:
                        print(source)
                        if 'data_set' in source.keys():
                            for set in source['data_set']:
                                if set['name'] == table_metadata_rule['source_data_set']:
                                    this_source = source
                    for prop in this_source['source_table_properties']:
                        time.sleep(1)
                        if prop['tblName'] == table_metadata_rule['source_data_set']:
                            this_rec_count = prop['tblRecordCount']

                    dq_rules[table_metadata_rule['source_data_set']]['rules'].append(
                        {
                            "tblMetadataInfo": {
                                "schema": self.get_table_schema(this_source, table_metadata_rule['source_data_set']),
                                "count": this_rec_count
                            },
                            "ruleType": "COLUMN_NAMES",
                            "operator": "=",
                            "tblName": table_metadata_rule['source_data_set'],
                            "creationTime": int(time.time()*1000),
                            "level": "Warning",
                            "expectedPercent": 100
                        }
                    )
                time.sleep(1)
                if 'data_types' in table_metadata_rule.keys() and table_metadata_rule['data_types']:
                    this_source = None
                    for source in sources:
                        print(source)
                        if 'data_set' in source.keys():
                            for set in source['data_set']:
                                if set['name'] == table_metadata_rule['source_data_set']:
                                    this_source = source

                    for prop in this_source['source_table_properties']:
                        time.sleep(1)
                        if prop['tblName'] == table_metadata_rule['source_data_set']:
                            this_rec_count = prop['tblRecordCount']

                    dq_rules[table_metadata_rule['source_data_set']]['rules'].append(
                        {
                            "tblMetadataInfo": {
                                "schema": self.get_table_schema(this_source, table_metadata_rule['source_data_set']),
                                "count": this_rec_count
                            },
                            "ruleType": "DATA_TYPES",
                            "operator": "=",
                            "tblName": table_metadata_rule['source_data_set'],
                            "creationTime": int(time.time()*1000),
                            "level": "Warning",
                            "expectedPercent": 100
                        }
                    )
                time.sleep(1)
                if 'new_columns' in table_metadata_rule.keys() and table_metadata_rule['data_types']:
                    this_source = None
                    for source in sources:
                        print(source)
                        if 'data_set' in source.keys():
                            for set in source['data_set']:
                                if set['name'] == table_metadata_rule['source_data_set']:
                                    this_source = source
                    for prop in this_source['source_table_properties']:
                        time.sleep(1)
                        if prop['tblName'] == table_metadata_rule['source_data_set']:
                            this_rec_count = prop['tblRecordCount']

                    dq_rules[table_metadata_rule['source_data_set']]['rules'].append(
                        {
                            "tblMetadataInfo": {
                                "schema": self.get_table_schema(this_source, table_metadata_rule['source_data_set']),
                                "count": this_rec_count
                            },
                            "ruleType": "ADD_NEW_COLUMN",
                            "operator": "=",
                            "tblName": table_metadata_rule['source_data_set'],
                            "creationTime": int(time.time()*1000),
                            "level": "Warning",
                            "expectedPercent": 100
                        }
                    )
                time.sleep(1)
                if 'removed_columns' in table_metadata_rule.keys() and table_metadata_rule['data_types']:
                    this_source = None
                    this_rec_count = None
                    for source in sources:
                        print(source)
                        if 'data_set' in source.keys():
                            for set in source['data_set']:
                                if set['name'] == table_metadata_rule['source_data_set']:
                                    this_source = source
                    for prop in this_source['source_table_properties']:
                        time.sleep(1)
                        if prop['tblName'] == table_metadata_rule['source_data_set']:
                            this_rec_count = prop['tblRecordCount']

                    dq_rules[table_metadata_rule['source_data_set']]['rules'].append(
                        {
                            "tblMetadataInfo": {
                                "schema": self.get_table_schema(this_source, table_metadata_rule['source_data_set']),
                                "count":  this_rec_count
                            },
                            "ruleType": "DROP_COLUMN",
                            "operator": "=",
                            "tblName": table_metadata_rule['source_data_set'],
                            "creationTime": int(time.time()*1000),
                            "level": "Warning",
                            "expectedPercent": 100
                        }
                    )
        # multi_source_sql rules
        if 'multi_source_sql_rules' in job.keys():
            for multi_source_sql_rule in job['multi_source_sql_rules']:
                time.sleep(1)
                if multi_source_sql_rule['source_data_set'] not in dq_rules.keys():
                    dq_rules[multi_source_sql_rule['source_data_set']] = {
                        "rules": []
                    }
                if 'source_data_set' not in multi_source_sql_rule.keys() or 'rules' not in multi_source_sql_rule.keys():
                                    raise APIInvocationError("source_data_set or rules  key is not present for multi_source_sql_rule")

                else:
                    for rule in multi_source_sql_rule['rules']:
                        time.sleep(1)
                        dq_rules[multi_source_sql_rule['source_data_set']]['rules'].append(
                            {
                                "ruleType": "SQL",
                                "column": "N/A",
                                "sql": rule['query'],
                                "creationTime": int(time.time()*1000),
                                "level": "Warning",
                                "name": rule['name'],
                                "executeSql": "SPARK",
                                "keyCols": rule.get("columns", [])
                            }
                        )

        # foreign_key_validaion rules
        condition ={
            "should exist": "exists"
        }
        if 'foreign_key_validation_rules' in job.keys():
            for foreign_key_validation_rules in job['foreign_key_validation_rules']:
                time.sleep(1)
                if foreign_key_validation_rules['main_table'] not in dq_rules.keys():
                    dq_rules[foreign_key_validation_rules['main_table']] = {
                        "rules": []
                    }
                dq_rules[foreign_key_validation_rules['main_table']]['rules'].append(
                    {
                        "ruleType": "FOREIGN_KEY_VALIDATION",
                        "column": "N/A",
                        "level": "Warning",
                        "creationTime": int(time.time()*1000),
                        "operator": "=",
                        "expectedPercent": 100,
                        "description": "Unique",
                        "name": foreign_key_validation_rules['name'],
                        "foreignKeyValidate": {
                            "condition": condition[foreign_key_validation_rules['condition']],
                            "mainTable": foreign_key_validation_rules['main_table'],
                            "mainTblCol": foreign_key_validation_rules['main_table_column'],
                            "lookupTbl": foreign_key_validation_rules['look_up_table'],
                            "lookupCol": foreign_key_validation_rules['look_up_table_column']
                        }
                    }
                )

        # good bad split rules
        if 'good_bad_split' in job.keys():
            for good_bad_split in job['good_bad_split']:
                time.sleep(1)
                if good_bad_split['source_data_set'] not in dq_rules.keys():
                    dq_rules[good_bad_split['source_data_set']] = {
                        "rules": []
                    }
                if 'source_data_set' not in good_bad_split.keys():
                                    raise APIInvocationError("source_data_set key is not present for multi_source_sql_rule")

                else:
                    good_data = good_bad_split.get('good_data')
                    bad_data = good_bad_split.get('bad_data')
                    dq_rules[good_bad_split['source_data_set']]['dqGood'] = {
                        "modeMap": {
                            "person_info_good": good_data.get('write_operation', "overwrite")
                        },
                        "sortColumns": good_data.get('sort_columns', []),
                        "srcDestTableMap": {
                            good_bad_split['source_data_set']: good_bad_split['source_data_set'] + "_good"
                        },
                        "tableNamesList": {
                            good_bad_split['source_data_set']: None
                        },
                        "targetFileSource": {
                            "datasetDelimiter": good_data['delimiter'],
                            "datasetFormat": good_data['file_format'].upper(),
                            "datasetPath":  good_data['file_path'],
                            "header":  good_data['header'],
                        } if good_data.get('file_format') in ['csv', 'CSV']
                        else{
                            "datasetFormat": good_data['file_format'].upper(),
                            "datasetPath":  good_data['file_path'],
                        }
                        if good_data.get('file_format') in ['parquet']
                        else{
                            "multiLine": good_data['multiline'],
                            "datasetFormat": good_data['file_format'].upper(),
                            "datasetPath":  good_data['file_path'],
                        }if good_data.get('file_format') in ['json']
                        else {
                            "startingPosition": good_data.get('starting_position','A1'),
                            "datasetFormat": good_data['file_format'].upper(),
                            "datasetPath":  good_data['file_path'],
                            "header":  good_data['header'],
                        }
                    }
                    if good_data.get('sftp_type'):
                        dq_rules[good_bad_split['source_data_set']]['dqGood']['targetFileSource']['jdbcData'] = {
                            "connectionName": good_data.get('sftp_type')
                        }
                    dq_rules[good_bad_split['source_data_set']]['dqBad'] = {
                        "modeMap": {
                            "person_info_good": bad_data.get('write_operation', "overwrite")
                        },
                        "sortColumns": bad_data.get('sort_columns', []),
                        "srcDestTableMap": {
                            good_bad_split['source_data_set']: good_bad_split['source_data_set'] + "_good"
                        },
                        "tableNamesList": {
                            good_bad_split['source_data_set']: None
                        },
                        "targetFileSource": {
                            "datasetDelimiter": bad_data['delimiter'],
                            "datasetFormat": bad_data['file_format'].upper(),
                            "datasetPath": bad_data['file_path'],
                            "header": bad_data['header'],
                        } if bad_data.get('file_format') in ['csv', 'CSV']
                        else {
                            "datasetFormat": bad_data['file_format'].upper(),
                            "datasetPath": bad_data['file_path'],
                        }
                        if bad_data.get('file_format') in ['parquet']
                        else {
                            "multiLine": bad_data['multiline'],
                            "datasetFormat": bad_data['file_format'].upper(),
                            "datasetPath": bad_data['file_path'],
                        } if bad_data.get('file_format') in ['json']
                        else {
                            "startingPosition": bad_data.get('starting_position', 'A1'),
                            "datasetFormat": bad_data['file_format'].upper(),
                            "datasetPath": bad_data['file_path'],
                            "header": bad_data['header'],
                        }
                    }
                    if bad_data.get('sftp_type'):
                        dq_rules[good_bad_split['source_data_set']]['dqGood']['targetFileSource']['jdbcData'] = {
                            "connectionName": bad_data.get('sftp_type')
                        }
                    dq_rules[good_bad_split['source_data_set']]['dqSplitType'] = "GOOD_AND_BAD"




        edit_data['dqRules'] = dq_rules

        flow_desc = job['name']
        params = {
            "executorCores": job['execution_params']['number_of_cores'] if 'execution_params' in job.keys() and
                             'number_of_cores' in job['execution_params'].keys() else default_number_of_cores,
            "executorMemory": job['execution_params']['memory_per_process'] if 'execution_params' in job.keys() and
                             'memory_per_process' in job['execution_params'].keys() else default_memory_per_process,
            "email": [],
            "flowName": flow_name,
            "flowDesc": flow_desc,
            "hadoop": False,
            "livy": False,
            "numExecutors": job['execution_params']['number_of_process'] if 'execution_params' in job.keys() and
                             'number_of_process' in job['execution_params'].keys() else default_number_of_process,
            "root": "component1",
            "executeOn": job['execution_params']['execute_on'].upper() if 'execution_params' in job.keys() and
                             'execute_on' in job['execution_params'].keys() else default_execute_on,
            "API_APP_BUILD": "Jun 5, 2021 11:37:08 PM",
            "UI_VERSION": "176-4c84765dc5",
            "APP_VERSION": "589-2caf3fb81d",
            "LicensedName": "microsoft",
            "flowType": "DATA QUALITY",
            "jobMap": {
                "component0": {
                    "dependentJob": True,
                    "type": "InputSource",
                    "description": "data_quality_job",
                    "position": {
                        "x": 259,
                        "y": 168
                    },
                    "id": "component0",
                    "sourceData": source_data
                },
                "component1": {
                    "dependentJob": True,
                    "dependsOn": [
                        "component0"
                    ],
                    "jobType": "DATA_QUALITY",
                    "type": "DataQuality",
                    "editData": edit_data,
                    "description": "any name",
                    "position": {
                        "x": 469,
                        "y": 294
                    },
                    "id": "component1"
                }



            }
        }
        # Data profile flag
        if 'config' in job.keys() and 'data_profile' in job['config'].keys() and job['config']['data_profile']:
            params['jobMap']['component1']["keyValueProps"] = {
                "DATA_PROFILE": "true"
            }
        # Random input sample
        if 'config' in job.keys() and 'random_input_sample' in job['config'].keys():
            if 'keyValueProps' in params['jobMap']['component1'].keys():
                params['jobMap']['component1']["keyValueProps"]['RANDOM_SAMPLE'] = str(job['config']['random_input_sample'])

            else:
                params['jobMap']['component1']["keyValueProps"] = {
                    "RANDOM_SAMPLE": str(job['config']['random_input_sample'])
                }


        url_parms = {
            "dmsProjectId": self.project_id,
            "project_name": flow_name,
            "project_description": flow_desc,
            "session_id": session_id,
            "session.id": session_id,

        }
        params = json.dumps(params)
        params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])
        print('check:')
        print(params)

        try:
            logging.debug("Creating data quality flow")
            response = requests.post(f"{self.url}/myapp/dvFlowUpload?{params_str}", data=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while creating new compare flow")

    def __refresh_session_id(self):
        """ Returns session Id
        :return: str session id
        """
        payload = f"action=login&username={self.username}&password={self.password}"
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        try:
            response = requests.post(f'{self.url}/myapp', headers=headers, data=payload)
            self.__session_id = response.json().get("session_id")
            self.auth_token = response.json().get("token")
        except Exception:
            raise APIInvocationError("Error was raised while getting session id")

    def __get_project_id(self):
        """ Returns DMS Project ID
        :return: str dms project id
        """
        url_parms = {
            "groupName": self.proj_group_name,
        }
        headers = {"sessionid": self.session_id}
        params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])
        try:
            logging.debug("Getting project Id")
            response = requests.get(f"{self.url}/dms/projects?{params_str}",headers=headers)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            self.__project_id = [data['id'] for data in response.json() if data['name'] == self.project_name][0]
        except Exception:
            raise APIInvocationError("Error was raised while getting DMS project ID")

    def get_table_grouping(self, tables_mapping, source: dict, target: dict, job: dict):
        default_small_table_limit = 12
        default_medium_table_limit = 7
        default_large_table_limit = 3

        large_table_limit = job['grouping_params']['large_table_limit'] \
            if 'grouping_params' in job.keys() and 'large_table_limit' in job['grouping_params'].keys()\
            else default_large_table_limit

        medium_table_limit = job['grouping_params']['medium_table_limit'] \
            if 'grouping_params' in job.keys() and 'medium_table_limit' in job['grouping_params'].keys() \
            else default_medium_table_limit

        small_table_limit = job['grouping_params']['small_table_limit'] \
            if 'grouping_params' in job.keys() and 'small_table_limit' in job['grouping_params'].keys() \
            else default_small_table_limit

        params = {
                    "srcData": {
                        "connectionName": source['connection_name'],
                        "databaseName": source['schema_name'],
                        "schema": source['schema_name'],
                        "currentUserGroup": self.proj_group_name
                    },
                    "targetData": {
                        "connectionName": target['connection_name'],
                        "databaseName": target['schema_name'],
                        "schema": target['schema_name'],
                        "currentUserGroup": self.proj_group_name
                    },
                    "tableMapping": [
                        {
                            "srcTable": elements[0],
                            "targetTable": elements[1]
                        }for elements in tables_mapping
                    ],
                    "smallTblSizeLimit": small_table_limit,
                    "mediumTblSizeLimit": medium_table_limit,
                    "largeTblSizeLimit": large_table_limit,
                    "factorSrc": 'true',
                    "sortTableByName": 'true'
                }
        headers = {
            "sessionid": self.session_id
        }
        try:
            logging.debug("Getting table grouping")
            response = requests.post(f"{self.url}/dmsspark/group/tables/", headers=headers, json=params)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            response = response.json()
            flow_list = []
            for key in response.keys():
                for group in response[key]:
                    flow_element = []
                    for element in group:
                        flow_element.append((element['srcTable'], element['targetTable']))

                    flow_list.append((flow_element,key))
            return flow_list
        except Exception:
            raise APIInvocationError("Error was raised while grouping")

    def execute_batch(self, batch_id):
        params = [{"groupId": self.proj_group_name, "parameters": []}]
        session_id = self.session_id
        headers = {
            "sessionid": session_id,
            "session_id": session_id,
            "Content-Type": 'application/json'
        }
        try:
            logging.debug("Executing batch")
            response = requests.post(f"{self.url}/dms/projects/{self.project_id}/batches/{batch_id}/execute?",
                                     json=params, headers=headers)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
            return response.json()
        except Exception:
            raise APIInvocationError("Error was raised while grouping")

    def upload_file(self, file_path: str, name_list: list):
        path = os.path.normpath(file_path)
        files = {'file': (path.split(os.sep)[-1], open(file_path, 'rb'))}
        response = None
        for name in name_list:
            values = {'ajax': 'uploadfile',
                      'session_id': self.session_id,
                      'file': name['name']}
            logging.debug("uploading file")
            response = requests.post(f"{self.url}/myapp/dvSyncServlet", files=files, data=values)
            logging.debug(f"URL: {response.request.url}")
            logging.debug(f"Header: {response.request.headers}")
            logging.debug(f"Body: {response.request.body}")
            logging.debug(f"Response: {response.json()}")
        return response.json()['filePath']





class CacheManager(object):
    """Cache manager for execution id"""

    @staticmethod
    def get_cache_path():
        """ Returns cache path
        :return: str
        """
        file_name = "execution_cache"
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), file_name)

    @staticmethod
    def write(value):
        """Stores execution id to cache
        :param value: str
        """
        with open(CacheManager.get_cache_path(), "w") as f:
            f.write(str(value))

    @staticmethod
    def read():
        """ Returns cashed execution id value
        :return: ste
        """
        try:
            with open(CacheManager.get_cache_path()) as f:
                return f.read()
        except FileNotFoundError:
            logging.warning("Cache file is not found")
            return ''
