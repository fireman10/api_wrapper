from argparse import ArgumentParser
import logging
import json
import jsonschema
import yaml
import os
import re

from dq.executors import TriggerFlowExecutor, GetBatchExecutionStatusExecutor, CompareSourcesExecutor, DataProfileExecutor, LoadYaml, \
    TriggerBatchExecutor, YamlResult, EtlTestingExecutor, DataQualityExecutor, schema_comapre, GetResultExecutor
from dq.utils import DQAPIExplorer

dirname = os.path.dirname(__file__)


log_format = "%(asctime)s [%(levelname)s] %(message)s"
logging.basicConfig(level=logging.INFO, format=log_format, handlers=[logging.StreamHandler()])
version = '01.00.00A005'

EXECUTORS = [
    LoadYaml,
    YamlResult,
    TriggerFlowExecutor,
    TriggerBatchExecutor,
    GetBatchExecutionStatusExecutor,
    GetResultExecutor
]


class ParamsValidationError(Exception):
    pass


class Job(object):
    def __init__(self, name: str, job_type: str):
        self.name = name
        self.job_type = job_type
        self.execution_params = {}
        self.mapping = []

    def add_mapping(self, source_name: str = None, source_data_set: str = None, target_name: str = None,
                    target_data_set: str = None):
        mapping = {}
        if source_name is not None:
            mapping['source_name'] = source_name
        if source_data_set is not None:
            mapping['source_data_set'] = source_data_set
        if target_name is not None:
            mapping['target_name'] = target_name
        if target_data_set is not None:
            mapping['target_data_set'] = target_data_set
        self.mapping.append(mapping)

    def configure_execution_params(self, parallel_flows: int = 1, execute_on: str = 'local',
                                   memory_per_process: str = '2g', number_of_cores: int = 1, number_of_process: int = 1):

        self.execution_params ={
            "parallel_flows": parallel_flows,
            "execute_on": execute_on,
            "memory_per_process": memory_per_process,
            "number_of_cores": number_of_cores,
            "number_of_process": number_of_process
        }


class DataQualityJob(Job):
    def __init__(self, name: str, random_input_sample: int = None, data_profile: bool = False):
        super().__init__(name, "data quality")
        config = {
            'data_profile' : data_profile
        }
        if random_input_sample is not None:
            config['random_input_sample'] = random_input_sample

        self.config = config
        self. column_rules = []
        self.sql_rules = []
        self.multi_source_sql_rules = []
        self.foreign_key_validation_rules = []
        self.table_metadata_rules = []
        self.good_bad_split = []

    def add_rule(self, rule):
        if isinstance(rule, ColumnRule):
            self.column_rules.append(rule.__dict__)
        if isinstance(rule, SqlRule):
            self.sql_rules.append(rule.__dict__)
        if isinstance(rule, MultiSourceSqlRule):
            self.multi_source_sql_rules.append(rule.__dict__)
        if isinstance(rule, ForeignKeyValidationRule):
            self.foreign_key_validation_rules.append(rule.__dict__['rules'])
        if isinstance(rule, TableMetadataRule):
            self.table_metadata_rules.append(rule.__dict__['rules'])
        if isinstance(rule, GoodBadSplit):
            self.good_bad_split.append(rule.__dict__)


class DataProfileJob(Job):
    def __init__(self, name: str, enable_profile_beta: bool = False):
        super().__init__(name, "data profile")
        self.enable_profile_beta = enable_profile_beta


class Source(object):
    def __init__(self, name: str, source_type: str):
        self.name = name
        self.source_type = source_type
        self.data_set = []


class FileSource(Source):
    def __init__(self, name: str, source_format: str, source_path: str,
                 table_name: str, sft_type: str):
        super().__init__(name, "file")
        self.source_format = source_format
        self.source_path = source_path
        self.data_set = [{"name": table_name}]
        self.sft_type = sft_type


class SqlSource(Source):
    def __init__(self, name: str, connection_name: str, schema_name: str):
        super().__init__(name, "sql")
        self.connection_name = connection_name
        self.schema_name = schema_name

    def add_dataset(self, dataset_name: str, query: str):
        self.data_set.append({"name": dataset_name,
                              "query": query})


class TableSource(Source):
    def __init__(self, name: str, connection_name: str, schema_name: str):
        super().__init__(name, "table")
        self.connection_name = connection_name
        self.schema_name = schema_name

    def add_dataset(self, dataset: list):
        for name in dataset:
            self.data_set.append({"name": name})

    def get_dataset(self) -> list:
        return self.data_set


class CsvSource(FileSource):
    def __init__(self, name: str, source_path: str, delimiter: str, header: bool,
                table_name: str, sft_type: str = None, start_position: int = 1, multi_line: bool = False,
                drop_bad_records: bool = True, infer_schema: bool = False):
        super().__init__(name=name, source_format="CSV", source_path=source_path, table_name=table_name,
                         sft_type=sft_type)
        self.delimiter = delimiter
        self.header = header
        self.inferSchema = infer_schema
        self.start_position = start_position
        self.multi_line = multi_line
        self.drop_bad_records = drop_bad_records


class GoodBadSplit(object):
    def __init__(self, data_set):
        self.source_data_set = data_set
        self.good_data = {}
        self.bad_data = {}

    def add_csv_good_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None, header: bool = True, delimiter: str = ','):
        self.good_data ={
            'file_format': 'csv',
            'file_path': file_path,
            'write_operation': write_operation,
            'header': header,
            'delimiter': delimiter
        }

        if sort_columns is not None:
            self.good_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.good_data['sftp_type'] = sftp_type

    def add_excel_good_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None, header: bool = True, starting_position: str = 'A1'):
        self.good_data ={
            'file_format': 'excel',
            'file_path': file_path,
            'write_operation': write_operation,
            'starting_position': starting_position,
            'header': header
        }

        if sort_columns is not None:
            self.good_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.good_data['sftp_type'] = sftp_type

    def add_json_good_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None):
        self.good_data ={
            'file_format': 'json',
            'file_path': file_path,
            'write_operation': write_operation
        }

        if sort_columns is not None:
            self.good_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.good_data['sftp_type'] = sftp_type

    def add_parquet_good_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None):
        self.good_data ={
            'file_format': 'parquet',
            'file_path': file_path,
            'write_operation': write_operation
        }

        if sort_columns is not None:
            self.good_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.good_data['sftp_type'] = sftp_type

    def add_csv_bad_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None, header: bool = True, delimiter: str = ','):
        self.bad_data ={
            'file_format': 'csv',
            'file_path': file_path,
            'write_operation': write_operation,
            'header': header,
            'delimiter': delimiter
        }

        if sort_columns is not None:
            self.bad_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.bad_data['sftp_type'] = sftp_type

    def add_excel_bad_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None, header: bool = True, starting_position: str = 'A1'):
        self.bad_data ={
            'file_format': 'excel',
            'file_path': file_path,
            'write_operation': write_operation,
            'starting_position': starting_position,
            'header': header
        }

        if sort_columns is not None:
            self.bad_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.bad_data['sftp_type'] = sftp_type

    def add_json_bad_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None):
        self.bad_data ={
            'file_format': 'json',
            'file_path': file_path,
            'write_operation': write_operation
        }

        if sort_columns is not None:
            self.bad_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.bad_data['sftp_type'] = sftp_type

    def add_parquet_bad_split(self, file_path: str, write_operation: str = 'overwrite',
                           sort_columns: str = None, sftp_type: str = None):
        self.bad_data ={
            'file_format': 'parquet',
            'file_path': file_path,
            'write_operation': write_operation
        }

        if sort_columns is not None:
            self.bad_data['sort_columns'] = sort_columns
        if sftp_type is not None:
            self.bad_data['sftp_type'] = sftp_type


class ColumnRule(object):
    def __init__(self, data_set):
        self.source_data_set = data_set
        self.rules = []

    def add_rule(self, column: str, empty_check: bool = None, null_check_operator: str = None,
                 null_check_percent: str = None, unique_check_operator: str = None, unique_check_percent: str = None,
                 left_spaces: bool = None, right_spaces: bool = None, min_length: int = None, max_length: int = None,
                 sql: str = None, regular_expression: str = None):
        column_rule = {
            'column': column
        }
        if empty_check is not None:
            column_rule['empty_check'] = empty_check
        if null_check_operator is not None and null_check_percent is not None:
            column_rule['null_check'] = {
                "operator": null_check_operator,
                "expected_percent": null_check_percent
            }
        if unique_check_operator is not None and unique_check_percent is not None:
            column_rule['unique_check'] = {
                "operator": unique_check_operator,
                "expected_percent": unique_check_percent
            }
        if left_spaces is not None:
            column_rule['left_spaces'] = left_spaces
        if right_spaces is not None:
            column_rule['right_spaces'] = right_spaces
        if min_length is not None:
            column_rule['min_length'] = min_length
        if max_length is not None:
            column_rule['max_length'] = max_length
        if sql is not None:
            column_rule['sql'] = sql
        if regular_expression is not None:
            column_rule['regular_expression'] = regular_expression

        self.rules.append(column_rule)

    def get_rules(self) -> dict:
        return self.__dict__


class SqlRule(object):
    def __init__(self, data_set):
        self.source_data_set = data_set
        self.rules =[]

    def add_rule(self, name:str, query: str) -> None:
        self.rules.append({
            'query': query,
            'name': name
        })

    def get_rules(self) -> dict:
        return self.__dict__


class TableMetadataRule(object):
    def __init__(self, data_set):
        self.rule = {
            'source_data_set': data_set
        }

    def add_rule(self, minimum_records_count: int = None, maximum_records_count: int = None,
                 columns_name: bool = None, data_types: bool = None, new_columns: bool = None,
                 removed_columns: bool = None) -> None:

        if minimum_records_count is not None:
            self.rule['minimum_records_count'] = minimum_records_count
        if maximum_records_count is not None:
            self.rule['maximum_records_count'] = maximum_records_count
        if columns_name is not None:
            self.rule['columns_name'] = columns_name
        if data_types is not None:
            self.rule['data_types'] = data_types
        if new_columns is not None:
            self.rule['data_types'] = new_columns
        if removed_columns is not None:
            self.rule['data_types'] = removed_columns

    def get_rules(self) -> dict:
        return self.__dict__


class ForeignKeyValidationRule(object):
    def __init__(self):
        self.rules = []

    def add_rule(self, name: str, main_table: str, look_up_table: str, main_table_column: str, look_up_table_column: str,
                 condition: str = "should exist") -> None:
        self.rules.append({
            'name': name,
            'main_table': main_table,
            'look_up_table': look_up_table,
            'main_table_column': main_table_column,
            'look_up_table_column': look_up_table_column,
            'condition': condition,
        })

    def get_rules(self) -> dict:
        return self.__dict__


class MultiSourceSqlRule(object):
    def __init__(self, data_set):
        self.source_data_set = data_set
        self.rules =[]

    def add_rule(self, name:str, query: str) -> None:
        self.rules.append({
            'query': query,
            'name': name
        })


class ParquetSource(FileSource):
    def __init__(self, name: str, source_path: str, table_name: str, sft_type: str = None):
        super().__init__(name=name, source_format="PARQUET", source_path=source_path, table_name=table_name,
                         sft_type=sft_type)


class JsonSource(FileSource):
    def __init__(self, name: str, source_path: str, table_name: str, sft_type: str = None, multiline: str = True,
                flatten_data: str = False, columns: json = {}):
        super().__init__(name=name, source_format="PARQUET", source_path=source_path, table_name=table_name,
                         sft_type=sft_type)
        self.multiline = multiline
        self.flatten_data = flatten_data
        self.columns = columns


class ExcelSource(FileSource):
    def __init__(self, name: str, source_path: str, header: bool, table_name: str, sft_type: str = None,
                start_position: str = "A1", infer_schema: bool = False):
        super().__init__(name=name, source_format="CSV", source_path=source_path, table_name=table_name,
                         sft_type=sft_type)

        self.header = header
        self.inferSchema = infer_schema
        self.start_position = start_position


class XmlSource(FileSource):
    def __init__(self, name: str, source_path: str, table_name: str, sft_type: str = None, row_tag: str = None):
        super().__init__(name=name, source_format="PARQUET", source_path=source_path, table_name=table_name,
                         sft_type=sft_type)
        self.row_tag = row_tag


class DQ(object):

    def __init__(self, url: str, username: str, password: str, proj_group_name: str, project_name: str,
                 log_level: str = 'info'):
        self.executor = DQAPIExplorer(url, username, password, proj_group_name, project_name)
        self.sources = {}
        self.targets = {}
        self.jobs = []
        if log_level == 'debug':
            logging.getLogger().setLevel(logging.DEBUG)
            logging.debug(f'Api Wrapper Version: {version}')
        elif log_level == 'info':
            logging.getLogger().setLevel(logging.INFO)

    def execute_flow(self, project_id_list: list, success_emails=None, failure_emails=None,
                     cache_result: bool = True, job=None):
        """ Triggers dq Flow, returns response message id exists or all response
        :param project_id_list:str
        :param success_emails:optional[str]
        :param job:optional: dic
        :param failure_emails:optional[str]
        :param cache_result:bool
        :return: Any[str,dict]
        """
        return TriggerFlowExecutor.execute(self.executor, **{k: v for k, v in locals().items() if k != 'self'})

    def execute_batch(self, batch_id_list: list = None):
        """ Triggers DQ executions with the provided ID
        :param batch_id_list: list
        :return [dict]
        """
        return TriggerFlowExecutor.execute_batch(self.executor, batch_id_list)

    def get_batch_execution_status(self, batch_execution_id: str = None, extended: bool = False,
                                   timeout: int = 600, sleep: int = 15) -> dict:
        """ Returns execution result
        :param batch_execution_id:str
        :param extended:bool
        :param timeout:int
        :param sleep:int
        :return:Any[str, dict]
        """
        return GetBatchExecutionStatusExecutor.execute(self.executor, **{k: v for k, v in locals().items() if k != 'self'})

    def get_result(self, execution_id: int) -> dict:
        return GetResultExecutor.execute(self.executor, **{k: v for k, v in locals().items() if k != 'self'})

    def add_source(self, source):
        if not isinstance(source, dict):
            source = source.__dict__

        if source['name'] in self.sources.keys():
            logging.error(f"Source {source['name']} already Present")
            raise ParamsValidationError(f"Source {source['name']} already Present")
        else:
            self.sources[source['name']] = source

        if source['source_type'] == 'file' and not re.match("^s3", source['source_path']):
            source['source_path'] = self.executor.upload_file(source['source_path'], source['data_set'])

    def add_target(self, target: dict):
        if target['name'] in self.targets.keys():
            logging.error(f"Target {target['name']} already Present")
            raise ParamsValidationError(f"Target {target['name']} already Present")
        else:
            self.targets[target['name']] = target
        if target['target_type'] == 'file' and not re.match("^s3", target['target_path']):
            target['target_path'] = self.executor.upload_file(target['target_path'], target['data_set'])

    def add_job(self, job: dict) -> (int, int):
        if not isinstance(job, dict):
            job = job.__dict__
        job['project_ids'] = self.initialise_job(job)
        logging.info(f"flow created successfully, project ids: {job['project_ids']}")
        job['batch_id'], job['batch_exe_id'] = self.execute_flow(job['project_ids'], job=job)
        logging.info(f"flow with project ids: {job['project_ids']} executed in batch, batch Id:"
                     f" {job['batch_id']}, batch execution id : {job['batch_exe_id']}")
        self.jobs.append(job)
        return job['batch_id'], job['batch_exe_id']

    def initialise_job(self, job: dict):
        if job['job_type'] == 'data migration validation':
            return self.data_migration_validation(job)
        elif job['job_type'] == 'schema compare':
            return self.schema_comapre(job)
        elif job['job_type'] == 'data profile':
            return self.data_profile_validation(job)
        elif job['job_type'] == 'etl testing':
            return self.etl_testing(job)
        elif job['job_type'] == 'data quality':
            return self.data_quality(job)

    def fetch_dataset(self, source: TableSource):
        print(json.dumps(source.__dict__))
        return self.executor.get_tables(source.__dict__)

    def data_migration_validation(self, job: dict):
        return CompareSourcesExecutor.execute(self.executor, self.sources, self.targets, job)

    def schema_comapre(self, job: dict):
        return schema_comapre.execute(self.executor, self.sources, self.targets, job)

    def etl_testing(self, job: dict):
        return EtlTestingExecutor.execute(self.executor, self.sources, self.targets, job)

    def data_profile_validation(self, job: dict):
        return DataProfileExecutor.execute(self.executor, self.sources, job)

    def data_quality(self, job: dict):
        return DataQualityExecutor.execute(self.executor, self.sources, job)

    @staticmethod
    def validate_params(required_params: set, params: dict):
        if not required_params.issubset({k for k, v in params.items() if v}):
            raise ParamsValidationError(f"Following parameters are required {required_params.difference(params.keys())}")




class DQYaml(object):
    def __init__(self, file:str, url: str, username: str, password: str, log_level: str = 'info'):
        schema = json.load(open(os.path.join(dirname, "schema.py")))
        self.yml_content = yaml.load(open(file), Loader=yaml.FullLoader)
        try:
            jsonschema.validate(instance=self.yml_content, schema=schema)
        except jsonschema.exceptions.ValidationError as er:
            raise ParamsValidationError(er)

        self.dq = DQ(url=url, username=username, password=password, proj_group_name= self.yml_content['group'],
                     project_name=self.yml_content['project'], log_level=log_level)

        auto_mapping_flag = False
        # Auto Mapping
        for job in self.yml_content.get('job'):
            for mapping in job.get('mapping'):
                if mapping.get('auto_mapping'):
                    auto_mapping_flag = True
                    source = None
                    target = None
                    for yml_source in self.yml_content.get('source'):
                        if yml_source.get('name') == mapping.get('source_name'):
                            source = yml_source

                    for yml_target in self.yml_content.get('target'):
                        if yml_target.get('name') == mapping.get('target_name'):
                            target = yml_target
                    exclude_source_table = []
                    exclude_target_table = []
                    if mapping.get('exclude') and mapping.get('exclude').get('source_tables'):
                        exclude_source_table = mapping.get('exclude').get('source_tables')
                    if mapping.get('exclude') and mapping.get('exclude').get('target_tables'):
                        exclude_target_table = mapping.get('exclude').get('target_tables')

                    exclude_source_table = [x.casefold() for x in exclude_source_table]
                    exclude_target_table = [x.casefold() for x in exclude_target_table]

                    source_tables = self.dq.executor.get_tables(source)
                    target_tables = self.dq.executor.get_tables(target)

                    for source_table in source_tables:
                        # adding data set
                        if source.get('data_set'):
                            source['data_set'].append({"name": source_table})
                        else:
                            source['data_set'] = [{"name": source_table}]

                    for target_table in target_tables:
                        # adding data set
                        if target.get('data_set'):
                            target['data_set'].append({"name": target_table})
                        else:
                            target['data_set'] = [{"name": target_table}]

                        for source_table in source_tables:
                            if source_table.casefold() == target_table.casefold() and source_table.casefold() not in \
                                    exclude_source_table and target_table.casefold() not in exclude_target_table:
                                try:
                                    target_table_info = self.dq.executor.get_table_info(target['connection_name'],
                                                                                        target['schema_name'],
                                                                                        target_table)
                                    source_table_info = self.dq.executor.get_table_info(source['connection_name'],
                                                                                        source['schema_name'],
                                                                                        source_table)
                                    if (not source_table_info[source_table]['primaryKey'] and source_table_info[source_table]['tableSize'] >= mapping.get('non_key_max_size_threshold', 1000)) or (not target_table_info[target_table]['primaryKey'] and \
                                target_table_info[target_table]['tableSize'] >= mapping.get('non_key_max_size_threshold', 1000)):
                                        continue
                                    else:
                                        job['mapping'].append({
                                            "source_name": source.get('name'),
                                            "target_name": target.get('name'),
                                            "source_data_set": source_table,
                                            "target_data_set": target_table
                                        })
                                except Exception as e:
                                    logging.debug(f"Excluding table mapping because got an exception: {e}")
                    job['mapping'].remove(mapping)

        if auto_mapping_flag:
            logging.debug("Modified YAML for auto mapping of table")
            logging.debug(f"New yaml content: {self.yml_content}")

        # initializing sources
        [self.dq.add_source(source=source) for source in self.yml_content['source']if 'source' in self.yml_content.keys()]
        # initializing targets
        if 'target' in self.yml_content.keys():
            [self.dq.add_target(target=target) for target in self.yml_content.get('target')]
        # initialize the jobs
        [self.dq.add_job(job) for job in self.yml_content['job'] if 'job' in self.yml_content.keys()]

    def get_result_yaml(self, extended: bool = True, timeout: int = 300, sleep: int = 15):
        result = []
        for job in self.dq.jobs:
            result.append(self.dq.get_batch_execution_status(batch_execution_id=job['batch_exe_id'], extended=extended,
                                                             timeout=timeout, sleep=sleep))
        return result

    def get_job(self):
        return self.dq.jobs


def main():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(help="actions")
    for executor in EXECUTORS:
        executor(subparsers.add_parser(executor.parser_name())).init_cli_parser()

    args = vars(parser.parse_args())
    action = args.pop("which", "")
    if action not in [i.parser_name() for i in EXECUTORS]:
        parser.print_help()
    else:
        dqyaml = None
        dq = None
        if 'yml' in args.keys():
            dqyaml = DQYaml(args.pop('yml'), args.pop('url'), args.pop('username'), args.pop('password'),
                            log_level=args.pop('log_level'))
        else:
            dq = DQ(args.pop('url'), args.pop('username'), args.pop('password'), args.pop('group'),
                    args.pop('project'))

        if action == 'yml_result':
            logging.info(dqyaml.get_result_yaml(extended=args.get('extended'), timeout=args.get('timeout'),
                                                sleep=args.get('sleep')))
        elif action == 'load_yml':
            logging.info(dqyaml.get_job())
        elif action == 'result':
            logging.info(dq.get_batch_execution_status(extended=args.get('extended'), timeout=args.get('timeout'),
                                       sleep=args.get('sleep')))
        elif action == 'execute_flow':
            logging.info(dq.execute_flow(project_id_list=args.get('project_ids').split(',')))
        elif action == 'execute_batch':
            logging.info(dq.execute_batch(args.pop('batch_ids').split(',')))


if __name__ == "__main__":
    main()