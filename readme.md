# dq API wrapper

## Execution modes:

- import as python lib
- use as cli tool


## Build:

 - ```pip3 install --no-cache-dir -r requirements.txt```<br />
 - ```python3 setup.py bdist_wheel```<br />
will generate dist folder with dq-1.0.0-py3-none-any.whl in dist 

## Installation

### Install from whl file
You can use wheel generated with previous command<br />
 - ```cd dist```<br />
 - ```pip3 install dq-1.0.0-py3-none-any.whl --force-reinstall```

### Install from setup.py
```pip3 install -e .```

## Usage
After package has been installed, you can use it with command line interface.

## Execute in a docker container
docker run -it -v "$PWD":/usr/src/myapp python:3 sh

### Using cli

 - ```dq --help```
 - ```dq load_yml -h```
 - ```dq load_yml -U usr -P psswrd --url http://dev.dataq.io --yml /Users/usr/project/DataQ/conf/dq.yml --log_level debug```
 - ```dq yml_result -U usr -P psswrd --url http://dev.dataq.io --yml /Users/usr/project/DataQ/conf/dq.yml```
 - ```dq yml_result -U usr -P psswrd --url http://dev.dataq.io --yml /Users/usr/project/DataQ/conf/dq.yml --sleep 5 --extended true timeout 120```
 - ```dq execute_flow -U usr -P psswrd --url http://dev.dataq.io --project SpaceX --group dev --project_ids 2274,2275```
 - ```dq execute_batch -U usr -P psswrd --url http://dev.dataq.io --project SpaceX --group dev --batch_ids 1800,1799```
 - ```dq result -U usr -P psswrd --url http://dev.dataq.io --project SpaceX --group dev --execution_id 869 --extended true```

 
### As python lib

#### Create and execute flow from YAML
```
from dq import DQYaml

dqyaml = DQYaml(file="/Users/usr/project/DataQ/conf/dq.yml", url='http://dev.dataq.io', username='usr', password='psswrd')
jobs = dqyaml.dq.jobs
results = dqyaml.get_result_yaml()
```
#### Get execution result for a flow
```
from dq import DQ
dq = DQ(url='http://dev.dataq.io', username='usr', password='psswrd', proj_group_name='dev', project_name='SpaceX')
result = dq.get_result('874')
```
#### Execute batch with batch ID
```
from dq import DQ
dq = DQ(url='http://dev.dataq.io', username='usr', password='psswrd', proj_group_name='dev', project_name='SpaceX')
dq.execute_batch(['1804', '1803'])
```
#### Execute flow in a batch
```
from dq import DQ
dq = DQ(url='http://dev.dataq.io', username='usr', password='psswrd', proj_group_name='dev', project_name='SpaceX')
dq.execute_flow(['2280', '2281'])

```

## Sample YAML File
```
Note:Grouping can be used inside a job with a single source and target where the source and target type can only be a table
```
```
group: dev
project: SpaceX
version: 1.0.0
source:
- name: source1
  source_type: table
  connection_name: tims
  schema_name: tims_db
  data_set:
  - name: courses_info
  - name: students_info
  - name: assessment_report
- name: source2
  source_type: sql
  connection_name: tims
  schema_name: tims_db
  data_set:
  - name: dataset1
    query: select * from assessment_report

- name: source3
  source_type: file
  source_format: CSV
  source_path: /user/home/gagupta/xyz.csv
  delimiter: ','
  header: true
  inferSchema: false
  data_set:
    - name: businessfinancial1

- name: source4
  source_type: file
  source_format: PARQUET
  source_path: s3a://dataqio/delme_temp.parquet
  data_set:
    - name: s3_parquet

- name: source5
  source_type: file
  source_format: JSON
  source_path: s3://dataqio/b.json
  multiline: true
  flatten_data: false
  data_set:
    - name: s3_json

- name: source6
  source_type: file
  source_format: EXCEL
  source_path: s3a://dataqio/delme_temp.xls
  header: true
  inferSchema: false
  data_set:
    - name: businessfinancial1

target:
- name: target1
  target_type: table
  connection_name: tims
  schema_name: tims_db
  data_set:
  - name: courses_info
  - name: students_info
  - name: assessment_report
  
- name: target2
  target_type: sql
  connection_name: tims
  schema_name: tims_db
  data_set:
  - name: dataset2
    query: select * from assessment_report
    
- name: target3
  target_type: file
  target_format: CSV
  target_path: /user/home/gagupta/xyz.csv
  delimiter: ','
  header: true
  inferSchema: false
  data_set:
    - name: businessfinancial2

- name: target4
  target_type: file
  target_format: PARQUET
  target_path: s3a://dataqio/delme_temp.parquet
  data_set:
    - name: s3_parquet2

- name: target5
  target_type: file
  target_format: JSON
  target_path: s3://dataqio/b.json
  multiline: true
  flatten_data: false
  data_set:
    - name: s3_json2

- name: target6
  target_type: file
  target_format: EXCEL
  target_path: s3a://dataqio/delme_temp.xls
  header: true
  inferSchema: false
  data_set:
    - name: businessfinancial1
    
job:
- name: YamlTesting
  job_type: data migration validation
  mapping:
  - source_name: source1
    target_name: target1
    source_data_set: courses_info
    target_data_set: courses_info

  - source_name: source1
    target_name: target1
    source_data_set: students_info
    target_data_set: students_info
    exclude:
      source_columns:
      - aadhar_Number
      
  - source_name: source1
    target_name: target1
    auto_mapping: true
    non_key_max_size_threshold: 1000
    exclude:
      source_tables:
        - table_name
      target_tables:
        - table_name

  - source_name: source2
    target_name: target2
    source_data_set: dataset1
    target_data_set: dataset2
    exclude:
      target_columns:
      - Clinical_medicine_tolerance_diff
      - Pharmacology_diff_name
      - Phsyciatry_not_nulls
      - Community_medicine_grtr_100
      - Primary_Contact_Number
      - Dentistry

  execution_params:
    parallel_flows: 1
    execute_on: local
    memory_per_process: 2g
    successful_emails: abc@gmail.com
    failure_emails: abd@gmail.com
    number_of_cores: 2
    number_of_process: 1


- name: YamlTesting1
  job_type: data migration validation
  mapping:
  - source_name: source1
    target_name: target1
    source_data_set: courses_info
    target_data_set: courses_info
  - source_name: source1
    target_name: target1
    source_data_set: assessment_report
    target_data_set: assessment_report
    exclude:
      target_columns:
      - Clinical_medicine_tolerance_diff
      - Pharmacology_diff_name
      - Phsyciatry_not_nulls
      - Community_medicine_grtr_100
      - Primary_Contact_Number
      - Dentistry
  - source_name: source1
    target_name: target1
    source_data_set: students_info
    target_data_set: students_info
    exclude:
      source_columns:
      - aadhar_Number

  grouping: true
  grouping_params:
    large_table_limit: 3
    medium_table_limit: 7
    small_table_limit: 2

  execution_params:
    parallel_flows: 1
    execute_on: local
    memory_per_process: 2g
    number_of_cores: 2
    number_of_process: 1
    
- name: YamlTestingDP1
  job_type: data profile
  mapping:
  - source_name: source1
    source_data_set: courses_info

  execution_params:
    parallel_flows: 1
    execute_on: local
    memory_per_process: 2g
    number_of_cores: 2
    number_of_process: 1


- name: yml_data_quality
  job_type: data quality
  mapping:
    - source_name: source1
      source_data_set: courses_info
    - source_name: source1
      source_data_set: students_info

  config:
    random_input_sample: 100
    data_profile: true

  column_rules:
    - source_data_set: courses_info
      rules:
      - column: code
        empty_check: true
        null_check:
          operator: '='
          expected_percent: 80
        unique_check:
          operator: '>'
          expected_percent: 90
        left_spaces: true
        right_spaces: true
        min_length: 10
        max_length: 100
        min_value: 1
        max_value: 100
        sql: select * from code
        regular_expression: abc

    - source_data_set: students_info
      rules:
      - column: postcode
        null_check:
          operator: '='
          expected_percent: 80
        unique_check:
          operator: '>'
          expected_percent: 90
      - column: address
        left_spaces: true
        right_spaces: true

  sql_rules:
    - source_data_set: courses_info
      rules:
      - query: select * from courses_info
        name: t1
      - query: select code from courses_info
        name: t2
    - source_data_set: students_info
      rules:
      - query: select * from students_info
        name: t3

  table_metadata_rules:
    - source_data_set: courses_info
      minimum_records_count: 1
      maximum_records_count: 1000
      columns_name: true
      data_types: true
      new_columns: true
      removed_columns: true

    - source_data_set: students_info
      minimum_records_count: 2
      maximum_records_count: 100
      columns_name: true

  multi_source_sql_rules:
    - source_data_set: courses_info
      rules:
        - query: select * from courses_info
          name: t1
        - query: select code from courses_info
          name: t2
          columns:
            -info_p4
    - source_data_set: students_info
      rules:
        - query: select * from students_info
          name: t3

  foreign_key_validaion_rules:
    - name: t7
      main_table: courses_info
      look_up_table: students_info
      main_table_column: Courseid
      look_up_table_column: address
      condition: should exist
      
   good_bad_split:
    - source_data_set: person_info
      good_data:
        file_format: csv
        file_path: s3://example
        delimiter: ,
        header: true
        write_operation: overwrite
        sort_columns:
          - id
          - name
      bad_data:
        file_format: parquet
        file_path: s3://example
        write_operation: overwrite
        sort_columns:
          - id
          - name
    - source_data_set: person_info1
      good_data:
        file_format: json
        file_path: s3://example
        multiline: true
        write_operation: overwrite
        sort_columns:
          - id
          - name
      bad_data:
        file_format: parquet
        file_path: sftp
        header: true
        sftp_type: sftp_new_3
        starting_position: A1
        write_operation: overwrite
        sort_columns:
          - id
          - name

```


