from setuptools import setup
from dq.version import version

with open('requirements.txt') as requirements_txt:
    install_requires = requirements_txt.read().splitlines()

setup(
    name='dq',
    version=version,
    description='Python dq API executor',
    author='',
    author_email='test@test.com',
    packages=['dq'],
    install_requires=install_requires,
    entry_points={
        "console_scripts": [
            'dq=dq:main'
        ]
    }
)
